clear

% A matlab example for writing an input data file for FaSTMM 

%read geometry from text file ([radius,x,y,z,Re{m},Im{m}])
p=load('bpca1024.dat');
N_s = size(p,1);

% Coordinates of centers of spheres (3-by-N_s matrix) 
%(N_s = number of spheres)
coord = p(:,2:4)';

% radius of circumscribing sphere (1-by-N_s matrix)
radius = p(:,1)';

% Complex permittivity of spheres (1-by-N_s matrix)
param = ((p(:,5) + i*p(:,6)).^2).';


% T-matrix index (1-by-N_s matrix)
tind = -randi([1 2],1,N_s);  

% Rotation angles (Euler) for scatterers (3-by-N_s matrix)
angles = zeros(size(coord));

% write geometry to file
filename = 'geometry.h5'; 

hdf5write(filename,'/coord', coord);
hdf5write(filename,'/radius', radius,'WriteMode','append');
hdf5write(filename,'/param_r', real(param),'WriteMode','append');
hdf5write(filename,'/param_i', imag(param),'WriteMode','append');
hdf5write(filename,'/tind', uint32(tind),'WriteMode','append');
hdf5write(filename,'/angles',angles,'WriteMode','append');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Run FaSTMM e.g. ./FaSTMM -geometry_file geometry.h5 -S_out mueller.h5
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read Mueller matrix from file
file = 'mueller.h5';

M = hdf5read(file,'/mueller'); 

%columns of M
%[phi,theta,M11,M12,M13,M14,M21,M22,M23,M24,M31,M32,M33,M34,M41,M42,M43,M44]

%cross sections
crs = hdf5read(file,'/cross_sections'); 

%crs(1) = C_ext
%crs(2) = C_sca
%crs(3) = C_abs

module orientation_averaging
use common
use octtree
use solver
implicit none


contains

  subroutine rotate_geometry(sphere,halton_init, sphere_new)
    type (data_struct), intent(in) :: sphere(:)
    type (data_struct) :: sphere_new(:)
    
    integer :: i1, halton_init, i
    real(dp) :: rot(3,3), rot2(3,3), phi,theta
    integer :: max_loc(3), min_loc(3)
    real(dp) :: cc(3)

    do i1 = 1, size(sphere)
       theta = acos(2*halton_seq(halton_init, 2)-1)
       phi = halton_seq(halton_init, 3)*2*pi 
      
       rot = rotation_matrix_euler(phi, 0.0_dp,0.0_dp)
       rot2 = rotation_matrix_euler(0.0_dp, theta,0.0_dp)
       
       sphere_new(i1)%cp = matmul(rot2,matmul(rot,sphere(i1)%cp))
       
    end do

    
max_loc(1) =  maxloc(sphere_new(:)%cp(1),1)
max_loc(2) =  maxloc(sphere_new(:)%cp(2),1)
max_loc(3) =  maxloc(sphere_new(:)%cp(3),1)


min_loc(1) =  minloc(sphere_new(:)%cp(1),1)
min_loc(2) =  minloc(sphere_new(:)%cp(2),1)
min_loc(3) =  minloc(sphere_new(:)%cp(3),1)



cc(1) = ((sphere_new(max_loc(1))%cp(1) + sphere(max_loc(1))%r) + &
    (sphere_new(min_loc(1))%cp(1) - sphere(min_loc(1))%r)) / 2.0_dp

cc(2) = ((sphere_new(max_loc(2))%cp(2) + sphere(max_loc(2))%r) + &
    (sphere_new(min_loc(2))%cp(2) - sphere(min_loc(2))%r)) / 2.0_dp

cc(3) = ((sphere_new(max_loc(3))%cp(3) + sphere(max_loc(3))%r) + &
    (sphere_new(min_loc(3))%cp(3) - sphere(min_loc(3))%r)) / 2.0_dp
!print*, cc(1)

do i1 = 1, size(sphere)
   sphere_new(i1)%cp =  sphere_new(i1)%cp - cc
end do


    
  end subroutine rotate_geometry
  
subroutine orientation_ave(sphere, Tmat,  ifT, k, N_phi, N_th, N_ave, halton_init, S_ave, crs_tot)
  type (data_struct) :: sphere(:)
  type (Tmatrix), dimension(:), allocatable :: Tmat
  type (data_struct), allocatable :: sphere_new(:)
  type (level_struct), dimension(:), allocatable :: otree
  integer :: restart, max_iter, halton_init, max_level, t1, t2, rate, N_phi, N_th, N_ave, ori, th, phi, Nmax, ifT,i1
  real(dp) :: ka, tol, int, g, crs(4), crs_tot(5), Csca_int
  complex(dp) :: k
  real(dp), dimension(:,:), allocatable :: S_ave, mueller
  complex(dp), dimension(:), allocatable :: b_vec, b_vec2, xx, xx2
  complex(dp), dimension(:,:), allocatable :: jones

  
  allocate(S_ave(N_th,17))
  call system_clock(T1,rate)

  crs_tot(:) = 0.0_dp
  S_ave(:,:) = 0.0_dp
  Csca_int = 0.0_dp
  
  do ori = 1, N_ave
     
     allocate(sphere_new(size(sphere)))
     sphere_new = sphere
  
     call rotate_geometry(sphere,halton_init+ori, sphere_new)
     print*, 'build octree...' 
     call create_octtree(sphere_new, otree, dble(k), max_level)
  
     do i1 = 1,size(otree)
        print*, 'level',i1-1,size(otree(i1)%tree), ', dl=',real(otree(i1)%tree(1)%dl), 'Nmax', otree(i1)%tree(1)%Nmax
     end do
     
     ka = dble(k)*sqrt(3.0d0)/2.0d0 * otree(1)%tree(1)%dl
     Nmax = truncation_order(ka) 
  
     sphere(1)%ifT = ifT
     
     call rhs2_xy(Nmax, sphere_new, Tmat, b_vec, b_vec2, k)

     allocate(xx(size(b_vec)))
     allocate(xx2(size(b_vec)))
     
     print*, 'Estimated memory usage', 2*max_iter*sizeof(xx)/1024/1024,'MB' 
     
     call gmres_mlfmm2(sphere_new, otree, Tmat, k, b_vec,b_vec2, xx, xx2, 1.0d-4, 5, 100)

    

     print*, 'Post processing...'
     deallocate(b_vec,b_vec2)
     call inc_xy(Nmax, sphere_new, b_vec, b_vec2, k)


!*************************************************************

     call mueller_matrix(sphere_new, xx, xx2, b_vec, b_vec2, k, N_th, N_phi, Nmax, mueller, jones, crs)


     do phi = 1, N_phi
        do th = 1, N_th
           S_ave(th,:) = S_ave(th,:) + mueller(th + N_th*(phi-1), 2:18)/dble(N_phi)/dble(N_ave)
        end do
     end do
     crs_tot(1:4) = crs_tot(1:4) + crs/dble(N_ave) 
     deallocate(otree, sphere_new, xx, xx2, b_vec, b_vec2, mueller,jones)

     call system_clock(T2)
  
     print*, 'orientation:',ori,'/',N_ave, 'time', real(T2-T1) / real(rate),'s'

  end do

  !asymmetry parameter
  g= 0.0_dp
  do th = 1, N_th
     g = g + 2.0_dp*pi*sin(S_ave(th,1))/dble(N_th-1)*pi * S_ave(th,2) * cos(S_ave(th,1))
     Csca_int = Csca_int + 2.0_dp*pi*sin(S_ave(th,1))/dble(N_th-1)*pi * S_ave(th,2)
  end do
  
  crs_tot(4) = Csca_int
  crs_tot(5) = g/crs_tot(4)
  
  print*, 'Cext =', crs_tot(1)
  print*, 'Csca =', crs_tot(2)
  print*, 'Cabs =', crs_tot(3)
  print*, 'Csca_int =', crs_tot(4)
  print*, 'Asymmetry parameter =', crs_tot(5)


end subroutine orientation_ave



end module orientation_averaging

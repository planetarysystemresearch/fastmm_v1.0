module integration_points
use common
use gaussquad
implicit none

contains



subroutine gaussint(P, w, order)
real(dp), dimension(:), allocatable :: P
real(dp), dimension(:), allocatable :: w
integer, intent(in) :: order

allocate(P(order), w(order))

call cpquad(order,dble(0.0),"Legendre",w,P)

P = P/2.0d0+0.5d0
w = w/2.0d0

end subroutine gaussint


subroutine sample_points(P,w,M,N)
real(dp), dimension(:,:), allocatable :: P
real(dp), dimension(:), allocatable :: w
integer, intent(in) :: M,N 

real(dp), dimension(:), allocatable :: P_theta, w_theta 
integer :: i1, i2, j1
real(dp) :: phi
call gaussint(P_theta, w_theta, M)

allocate(P(3,M*N),w(M*N))

P_theta = pi * P_theta
w_theta = pi * w_theta

j1 = 1
do i1 = 1,N
   phi = dble(i1-1) * 2*pi / dble(N)
   do i2 = 1,M
      P(1,j1) = cos(phi) * sin(P_theta(i2))
      P(2,j1) = sin(phi) * sin(P_theta(i2))
      P(3,j1) = cos(P_theta(i2))
      w(j1) = W_theta(i2) / dble(N) * sin(P_theta(i2)) * 2 * pi
      
      j1 = j1 + 1
   end do
end do


end subroutine sample_points

end module 

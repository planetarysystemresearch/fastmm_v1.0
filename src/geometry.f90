module geometry
use common
! use ifport ! intel

implicit none

contains


subroutine init_random_seed()

      INTEGER :: i, n, clock
      integer :: seed1
      INTEGER, DIMENSION(:), ALLOCATABLE :: seed

      CALL RANDOM_SEED(size = n)
      ALLOCATE(seed(n))

     
      CALL SYSTEM_CLOCK(COUNT=clock)

      seed1 = abs( mod((clock*181)*((getpid()-83)*359), 104729)) 

      seed = seed1 + 37 * (/ (i - 1, i = 1, n) /)
     

      CALL RANDOM_SEED(PUT = seed)

      DEALLOCATE(seed)
    end subroutine init_random_seed

subroutine pack_spheres(N, dens, r, eps, Ntmax, coord, radius, param, angle, tindex)
real(dp) :: r, dens
complex(dp) ::eps 
integer :: N, las, i1, ok, Ntmax, tind

real(dp) :: rmax, vol, vol_tot, ran(3), normr, dis, pos(3), ran2(3), ran3
real(dp), dimension(3,N) :: coord
real(dp), dimension(3,N) :: angle
real(dp), dimension(N) :: radius
complex(dp), dimension(N) :: param
integer, dimension(N) :: tindex

vol = N * 4.0/3.0 * pi * r**3.0 
vol_tot = vol/dens
rmax = (3.0/4.0 * vol_tot / pi )**(1.0/3.0)
print*, 'Radius of medium', rmax
CALL init_random_seed()
 
las = 1
do while(las == 1)

   call random_number(ran)
   pos = (2*ran - 1) * rmax

   call random_number(ran2)

   normr = sqrt(dot_product(pos,pos))

   call random_number(ran3)
   tind = 1+floor((Ntmax-1)*ran3)
   
   if(normr+r <= rmax) then
      coord(:,1) = pos
      param(1) = eps
      radius(1) = r
      angle(:,1) = ran2* 2.0*pi
      tindex(1) = tind
      las = las + 1
   end if

end do
 


las = 1
do while(las < N)

   call random_number(ran)
   pos = (2*ran - 1) * rmax
   call random_number(ran2)
   normr = sqrt(dot_product(pos,pos))

   call random_number(ran3)
   tind = 1+floor((Ntmax)*ran3)
   !print*, tind
   
   if(normr+r <= rmax) then

      ok = 1
      do i1 = 1, las

         dis=sqrt(dot_product(pos-coord(:,i1),pos-coord(:,i1)))

         if(dis < 2*r) then         
           ok = 0
         end if
      end do

      if (ok == 1) then

         las = las + 1
         
         coord(:,las) = pos
         param(las) = eps
         radius(las) = r
         angle(:,las) = ran2*2.0*pi
         tindex(las) = tind
      end if


   end if

   
end do

end subroutine pack_spheres

subroutine pack_cylinder(N, dens, r, eps, Ntmax, coord2, radius, param, angle, tindex)
real(dp) :: r, dens, rmax2
complex(dp) ::eps 
integer :: N, las, i1, ok, Ntmax, tind

real(dp) :: rmax, vol, vol_tot, ran(3), normr, dis, pos(3), ran2(3), ran3, ratz
real(dp), dimension(3,N) :: coord, coord2
real(dp), dimension(3,N) :: angle
real(dp), dimension(N) :: radius
complex(dp), dimension(N) :: param
integer, dimension(N) :: tindex

ratz=0.25d0
vol = N * 4.0/3.0 * pi * r**3.0 ! spheres 
vol_tot = vol/dens !tot vol

rmax = (vol_tot * ratz / pi )**(1.0/3.0)

rmax2 = maxval([rmax, rmax/ratz/2])
print*, rmax, rmax/ratz, rmax2 

print*, 'Radius of medium', rmax
CALL init_random_seed()
 
las = 1
do while(las == 1)

   call random_number(ran)
   pos = (2*ran - 1) * rmax2

   call random_number(ran2)

   normr = sqrt(dot_product(pos(1:2),pos(1:2)))

   call random_number(ran3)
   tind = 1+floor((Ntmax-1)*ran3)
   
   if(normr+r <= rmax .and. abs(pos(3)) < rmax/ratz/2.0) then
      coord(:,1) = pos
      param(1) = eps
      radius(1) = r
      angle(:,1) = ran2* 2.0*pi
      tindex(1) = tind
      las = las + 1
   end if

end do
 


las = 1
do while(las < N)

   call random_number(ran)
   pos = (2*ran - 1) * rmax2
   call random_number(ran2)
   normr = sqrt(dot_product(pos(1:2),pos(1:2)))

   call random_number(ran3)
   tind = 1+floor((Ntmax)*ran3)
   !print*, tind
   
   if(normr+r <= rmax  .and. abs(pos(3)) < rmax/ratz/2) then

      ok = 1
      do i1 = 1, las

         dis=sqrt(dot_product(pos-coord(:,i1),pos-coord(:,i1)))

         if(dis < 2*r) then         
           ok = 0
         end if
      end do

      if (ok == 1) then

         las = las + 1
         
         coord(:,las) = pos
         param(las) = eps
         radius(las) = r
         angle(:,las) = ran2*2.0*pi
         tindex(las) = tind
      end if


   end if

   
end do

coord2(1,:) = coord(3,:)
coord2(2,:) = coord(2,:)
coord2(3,:) = coord(1,:)


end subroutine pack_cylinder


end module geometry

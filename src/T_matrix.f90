module T_matrix
use common
use solver
use translations
implicit none 

contains

subroutine compute_T_matrix(sphere, otree, Tmat, k, Nmax, tol, restart, maxit, Taa, Tab, Tba, Tbb)
type (data_struct), dimension(:) :: sphere
type (level_struct), dimension(:) :: otree
type (Tmatrix), dimension(:) :: Tmat
complex(dp) :: k
integer :: Nmax, maxit, restart 
real(dp) :: tol
integer :: i1, t1,t2,rate
complex(dp) :: a_in((Nmax + 1)**2-1), b_in((Nmax + 1)**2-1)
complex(dp) :: a_in2((Nmax + 1)**2-1), b_in2((Nmax + 1)**2-1)

complex(dp) :: a_nm((Nmax + 1)**2-1), b_nm((Nmax + 1)**2-1)
complex(dp) :: a_nm2((Nmax + 1)**2-1), b_nm2((Nmax + 1)**2-1)
complex(dp) :: Taa((Nmax + 1)**2-1, (Nmax + 1)**2-1)
complex(dp) :: Tbb((Nmax + 1)**2-1, (Nmax + 1)**2-1)
complex(dp) :: Tab((Nmax + 1)**2-1, (Nmax + 1)**2-1)
complex(dp) :: Tba((Nmax + 1)**2-1, (Nmax + 1)**2-1)
complex(dp), dimension(:), allocatable :: rhs, x, rhs2, x2

real(dp) :: orig(3)

a_in(:) = dcmplx(0.0,0.0)
b_in(:) = dcmplx(0.0,0.0)

orig(:) = 0.0
do i1 = 1, size(a_in)

  call system_clock(T1,rate)
  
  a_in(:) = dcmplx(0.0,0.0)
  b_in(:) = dcmplx(0.0,0.0)
  
  a_in(i1) = dcmplx(1.0,0.0)
  
  call cluster_rhs(Nmax, a_in, b_in, sphere, Tmat, rhs, k)
  
  a_in2(:) = dcmplx(0.0,0.0)
  b_in2(:) = dcmplx(0.0,0.0)

  b_in2(i1) = dcmplx(1.0,0.0)

  call cluster_rhs(Nmax, a_in2, b_in2, sphere, Tmat, rhs2, k)

  allocate(x(size(rhs)))
  allocate(x2(size(rhs2)))

  call gmres_mlfmm2(sphere, otree, Tmat, k, rhs,rhs2, x,x2, tol, restart, maxit)

  !call gmres(sphere, k, rhs, x, dble(1e-3), 4, 20) 
  call cluster_coeff(sphere, x, k, a_nm, b_nm, Nmax)
  call cluster_coeff(sphere, x2, k, a_nm2, b_nm2, Nmax)

  Taa(:,i1) = a_nm
  Tba(:,i1) = b_nm
  
  Tab(:,i1) = a_nm2
  Tbb(:,i1) = b_nm2
   


  call system_clock(T2)

  print*, 'T-matrix column:', i1, '/', size(a_in) ,', t =', real(T2-T1) / real(rate),'s'
  deallocate(x, rhs, x2, rhs2)
  
end do

end subroutine compute_T_matrix



end module T_matrix

module matvec
use common
use translations
use mie
use octtree
use omp_lib

implicit none

type data
   complex(dp), dimension(:,:), allocatable :: a_out, b_out
   complex(dp), dimension(:,:), allocatable :: a_ph, a_th, b_ph, b_th
end type data


contains

function L_order(Nmax) result(Lmax)
  integer :: Lmax, Nmax
  Lmax = 2*Nmax + 1

end function L_order

subroutine matvec_mlfmm_diag(sphere, otree, k, x, x_out, x2, x_out2)
type (data_struct), dimension(:) :: sphere
type (level_struct), dimension(:) :: otree
complex(dp), dimension(:) :: x , x_out, x2, x_out2
complex(dp) :: k

type (data), dimension(:), allocatable :: aggregates, aggregates2

integer :: sph1, sph2, Nspheres, Nmax, nm, s1, Nmax2, nm2
integer :: i1_a1, i1_a2, i1_b1, i1_b2, i2_a1, i2_a2, i2_b1, i2_b2, box, box2
integer :: sph1_loc, sph2_loc, box2_loc, i1, near_box, Nmax_box
integer :: Nmax_box1, Nmax_box2, L_max_box
complex(dp), dimension(:), allocatable :: a_nm, b_nm, c_nm, d_nm, T
complex(dp), dimension(:,:), allocatable :: a_loc, b_loc, a_out, b_out
complex(dp), dimension(:,:), allocatable :: a_box2, b_box2, a_box2_2, b_box2_2
complex(dp), dimension(:,:), allocatable :: c_box2, d_box2, c_box2_2, d_box2_2
complex(dp), dimension(:,:), allocatable :: a_box_loc, b_box_loc, a_box2_loc, b_box2_loc
complex(dp), dimension(:,:), allocatable :: aa_in, ab_in, aa_in2, ab_in2
real(dp) :: cp_box(3), cp_box2(3)
integer :: nm_box, nm_box2, max_level, level_ind, level, parent, neighbour, i2, i3
integer :: T1, T2, rate
integer :: mem, diag, nm_box_pw

complex(dp), allocatable :: Fth(:,:), Fph(:,:), Gth(:,:), Gph(:,:) 
real(dp), allocatable :: P0(:,:), w0(:)

diag = 0

call system_clock(T1,rate)

max_level = size(otree) - 1
level_ind = max_level + 1
Nspheres = size(sphere)


!*** Allocate aggregates *******

allocate(aggregates(max_level))
allocate(aggregates2(max_level))


Nmax_box = otree(max_level+1)%tree(1)%Nmax   
nm_box_pw = L_order(Nmax_box)**2 
nm_box = (Nmax_box+1)**2-1

if(diag == 0) then
   allocate(aggregates(max_level)%a_out(nm_box,size(otree(max_level+1)%tree)))
   allocate(aggregates(max_level)%b_out(nm_box,size(otree(max_level+1)%tree)))

   allocate(aggregates2(max_level)%a_out(nm_box,size(otree(max_level+1)%tree)))
   allocate(aggregates2(max_level)%b_out(nm_box,size(otree(max_level+1)%tree)))

else 
   allocate(aggregates(max_level)%a_th(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates(max_level)%a_ph(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates(max_level)%b_th(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates(max_level)%b_ph(nm_box_pw,size(otree(max_level+1)%tree)))

   allocate(aggregates2(max_level)%a_th(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates2(max_level)%a_ph(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates2(max_level)%b_th(nm_box_pw,size(otree(max_level+1)%tree)))
   allocate(aggregates2(max_level)%b_ph(nm_box_pw,size(otree(max_level+1)%tree)))
   
end if


!mem = sizeof(aggregates(max_level)%a_out)

if(max_level >= 3) then
   !print*,'not'
   do level = max_level, 3, -1
      Nmax_box2 = otree(level)%tree(1)%Nmax  ! low
      nm_box_pw = L_order(Nmax_box2)**2 !(Nmax_box2+1)**2-1 !low
      nm_box = (Nmax_box2+1)**2-1 !lo

      if(diag == 0) then
         allocate(aggregates(level-1)%a_out(nm_box,size(otree(level)%tree)))  
         allocate(aggregates(level-1)%b_out(nm_box,size(otree(level)%tree))) 

         allocate(aggregates2(level-1)%a_out(nm_box,size(otree(level)%tree)))  
         allocate(aggregates2(level-1)%b_out(nm_box,size(otree(level)%tree))) 

         mem = mem + sizeof(aggregates(level-1)%a_out)
      else

         allocate(aggregates(level-1)%a_th(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates(level-1)%a_ph(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates(level-1)%b_th(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates(level-1)%b_ph(nm_box_pw,size(otree(level)%tree)))
         
         allocate(aggregates2(level-1)%a_th(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates2(level-1)%a_ph(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates2(level-1)%b_th(nm_box_pw,size(otree(level)%tree)))
         allocate(aggregates2(level-1)%b_ph(nm_box_pw,size(otree(level)%tree)))
   
   
         mem = mem + 4*sizeof(aggregates(level-1)%a_ph)
      end if


   end do
end if


 
!*************** Near-zone interactions *******************************

!$omp parallel default(private) &
!$omp firstprivate(k, level_ind) &
!$omp shared(x_out,x_out2,x,x2, otree, sphere) 
!$omp do schedule(dynamic,1)

do box = 1, size(otree(level_ind)%tree)

   do sph1_loc = 1,  otree(level_ind)%tree(box)%N_source

      sph1 =  otree(level_ind)%tree(box)%sources(sph1_loc)
      Nmax = sphere(sph1)%Nmax
      nm = (Nmax+1)**2-1
 
      s1 = sphere(sph1)%ind_a2 - sphere(sph1)%ind_a1 + 1
      allocate(a_loc(s1,2), b_loc(s1,2))
      allocate(a_out(s1,2), b_out(s1,2))
      a_loc(:,:) = dcmplx(0.0,0.0)
      b_loc(:,:) = dcmplx(0.0,0.0)

      i1_a1 = sphere(sph1)%ind_a1
      i1_a2 = sphere(sph1)%ind_a2
      i1_b1 = sphere(sph1)%ind_b1
      i1_b2 = sphere(sph1)%ind_b2


      do box2_loc = 1,27

         box2 =   otree(level_ind)%tree(box)%near_neighbours(box2_loc)

         
         if(box2 == 0) exit
     
         do sph2_loc = 1,   otree(level_ind)%tree(box2)%N_source

            sph2 =  otree(level_ind)%tree(box2)%sources(sph2_loc)

            i2_a1 = sphere(sph2)%ind_a1
            i2_a2 = sphere(sph2)%ind_a2
            i2_b1 = sphere(sph2)%ind_b1
            i2_b2 = sphere(sph2)%ind_b2

            Nmax2 = sphere(sph2)%Nmax
            nm2 = (Nmax2+1)**2-1

            if(sph1 .ne. sph2) then
          

               call translate_xy(sphere(sph1)%cp-sphere(sph2)%cp, Nmax2, Nmax,&
                    k, [x(i2_a1:i2_a2),x2(i2_a1:i2_a2)], &
                    [x(i2_b1:i2_b2),x2(i2_b1:i2_b2)], a_out, b_out,1)


               a_loc(:,1) = a_loc(:,1) + a_out(:,1)  
               b_loc(:,1) = b_loc(:,1) + b_out(:,1)

               a_loc(:,2) = a_loc(:,2) + a_out(:,2)  
               b_loc(:,2) = b_loc(:,2) + b_out(:,2)

            end if

         end do

      end do

      x_out(i1_a1:i1_a2) = a_loc(:,1)
      x_out(i1_b1:i1_b2) = b_loc(:,1)

      x_out2(i1_a1:i1_a2) = a_loc(:,2)
      x_out2(i1_b1:i1_b2) = b_loc(:,2)


      deallocate(a_out, b_out)
      deallocate(a_loc, b_loc)
    
   end do
   
end do

!$omp end do
!$omp end parallel

call system_clock(T2)

if(sphere(1)%ifT == 0) print*, 'Near-zone done in ', real(T2-T1) / real(rate)

! *******************************************************
!
! mlfmm part
!
!*******************************************************
call system_clock(T1,rate)

!**** Aggregation at the finest level ****************



Nmax_box = otree(max_level+1)%tree(1)%Nmax   
nm_box = (Nmax_box+1)**2-1

allocate(a_box_loc(nm_box,2), b_box_loc(nm_box,2))
allocate(a_box2_loc(nm_box,2), b_box2_loc(nm_box,2))



if(diag .ne. 0) then
   call sample_points(P0,w0,L_order(Nmax_box),L_order(Nmax_box))
     
   allocate(Fth(size(w0),(Nmax_box+1)**2-1))
   allocate(Fph(size(w0),(nmax_box+1)**2-1))
   allocate(Gth(size(w0),(nmax_box+1)**2-1))
   allocate(Gph(size(w0),(nmax_box+1)**2-1))

   call sph2vswf(P0, Nmax_box, k, Fth, Fph, Gth, Gph)
end if

!$omp parallel default(private) &
!$omp firstprivate(k, max_level, Nmax_box, Fth, Fph, Gth, Gph, diag) &
!$omp shared(aggregates, aggregates2, x, x2, sphere, otree)
!$omp do schedule(dynamic,1)
do box = 1, size(otree(max_level+1)%tree)

   cp_box = otree(max_level+1)%tree(box)%cp

   a_box2_loc(:,:) = dcmplx(0.0,0.0)
   b_box2_loc(:,:) = dcmplx(0.0,0.0)
   
   do sph1_loc = 1, otree(max_level+1)%tree(box)%N_source

      sph1 = otree(max_level+1)%tree(box)%sources(sph1_loc)
 
       Nmax = sphere(sph1)%Nmax
       nm = (Nmax+1)**2-1

       i1_a1 = sphere(sph1)%ind_a1
       i1_a2 = sphere(sph1)%ind_a2
       i1_b1 = sphere(sph1)%ind_b1
       i1_b2 = sphere(sph1)%ind_b2
      
       call translate_xy(cp_box-sphere(sph1)%cp, Nmax, Nmax_box, k, &
            [x(i1_a1:i1_a2),x2(i1_a1:i1_a2)], &
            [x(i1_b1:i1_b2),x2(i1_b1:i1_b2)], a_box_loc, b_box_loc,0)
         

       a_box2_loc = a_box2_loc + a_box_loc
       b_box2_loc = b_box2_loc + b_box_loc

   end do
   
   if(diag == 0) then
      aggregates(max_level)%a_out(:,box) = a_box2_loc(:,1)
      aggregates(max_level)%b_out(:,box) = b_box2_loc(:,1)
   
      aggregates2(max_level)%a_out(:,box) = a_box2_loc(:,2)
      aggregates2(max_level)%b_out(:,box) = b_box2_loc(:,2)
      
   else
      aggregates(max_level)%a_th(:,box) = matmul(Fth, a_box2_loc(:,1))
      aggregates(max_level)%a_ph(:,box) = matmul(Fph, a_box2_loc(:,1))
      aggregates(max_level)%b_th(:,box) = matmul(Fth, b_box2_loc(:,1))
      aggregates(max_level)%b_ph(:,box) = matmul(Fph, b_box2_loc(:,1))
          
      aggregates2(max_level)%a_th(:,box) = matmul(Fth, a_box2_loc(:,2))
      aggregates2(max_level)%a_ph(:,box) = matmul(Fph, a_box2_loc(:,2))
      aggregates2(max_level)%b_th(:,box) = matmul(Fth, b_box2_loc(:,2))
      aggregates2(max_level)%b_ph(:,box) = matmul(Fph, b_box2_loc(:,2))

   end if

end do
!$omp end do
!$omp end parallel

deallocate(a_box_loc,b_box_loc)
deallocate(a_box2_loc,b_box2_loc)
if(diag .ne. 0) deallocate(P0,w0)


!******** Aggregation to lower levels ****************!
! high to low 
!
if(max_level >= 3) then
  !print*,'no'
   do level = max_level, 3, -1

      Nmax_box1 = otree(level)%tree(1)%Nmax ! parent
      Nmax_box2 = otree(level+1)%tree(1)%Nmax ! child 

      nm_box = (Nmax_box1+1)**2-1 

      allocate(a_box_loc(nm_box,2),b_box_loc(nm_box,2)) !low
    
     
      aggregates(level-1)%a_out(:,:) = dcmplx(0.0,0.0) ! low
      aggregates(level-1)%b_out(:,:) = dcmplx(0.0,0.0) ! low

      aggregates2(level-1)%a_out(:,:) = dcmplx(0.0,0.0) ! low
      aggregates2(level-1)%b_out(:,:) = dcmplx(0.0,0.0) ! low


      !$omp parallel default(private) &
      !$omp firstprivate(Nmax_box2, k, level) &
      !$omp shared(aggregates, aggregates2, otree) 
      !$omp do schedule(dynamic,1) 
    
      do box = 1, size(otree(level)%tree) ! high parent box

         cp_box = otree(level)%tree(box)%cp 
         Nmax_box1 = otree(level)%tree(box)%Nmax

         do i1 = 1, 8 ! children

            box2 = otree(level)%tree(box)%children(i1)   
            if(box2 == 0) exit
            cp_box2 = otree(level+1)%tree(box2)%cp 
         
            
            ! high to low translation 
            call translate_xy(cp_box-cp_box2, Nmax_box2, Nmax_box1, k, &
              [aggregates(level)%a_out(:,box2),aggregates2(level)%a_out(:,box2)], &
              [aggregates(level)%b_out(:,box2),aggregates2(level)%b_out(:,box2)], &
              a_box_loc, b_box_loc,0)
            

            !call translate_xy(cp_box-cp_box2, Nmax_box2, Nmax_box1, k, &
            !     [aa_in(:,box2),aa_in2(:,box2)], &
            !     [ab_in(:,box2),ab_in2(:,box2)], &
            !     a_box_loc, b_box_loc,0)




            aggregates(level-1)%a_out(:,box) = aggregates(level-1)%a_out(:,box) + a_box_loc(:,1)
            aggregates(level-1)%b_out(:,box) = aggregates(level-1)%b_out(:,box) + b_box_loc(:,1)
   
            aggregates2(level-1)%a_out(:,box) = aggregates2(level-1)%a_out(:,box) + a_box_loc(:,2)
            aggregates2(level-1)%b_out(:,box) = aggregates2(level-1)%b_out(:,box) + b_box_loc(:,2)

         end do
      end do

      !$omp end do
      !$omp end parallel

      !deallocate(aa_in, ab_in, aa_in2, ab_in2)

      deallocate(a_box_loc, b_box_loc)
     
      
   end do
end if

call system_clock(T2)
if(sphere(1)%ifT == 0) print*, 'Aggregation done in', real(T2-T1) / real(rate)




!********** Translation **************************************


do level = max_level,2,-1
   call system_clock(T1,rate)
   Nmax_box = otree(level+1)%tree(1)%Nmax   
   nm_box = (Nmax_box+1)**2-1
   nm_box_pw = L_order(Nmax_box)**2

   if(diag == 0) then
      allocate(a_box2_loc(nm_box,2), b_box2_loc(nm_box,2))

      allocate(a_box2(nm_box, size(otree(level+1)%tree)))
      allocate(b_box2(nm_box, size(otree(level+1)%tree)))
      
      allocate(a_box2_2(nm_box, size(otree(level+1)%tree)))
      allocate(b_box2_2(nm_box, size(otree(level+1)%tree)))
   
      a_box2(:,:) = dcmplx(0.0,0.0)
      b_box2(:,:) = dcmplx(0.0,0.0)

      a_box2_2(:,:) = dcmplx(0.0,0.0)
      b_box2_2(:,:) = dcmplx(0.0,0.0)

   else
   
      
      allocate(a_box2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(b_box2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(c_box2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(d_box2(nm_box_pw, size(otree(level+1)%tree)))

      allocate(a_box2_2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(b_box2_2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(c_box2_2(nm_box_pw, size(otree(level+1)%tree)))
      allocate(d_box2_2(nm_box_pw, size(otree(level+1)%tree)))

      a_box2(:,:) = dcmplx(0.0,0.0)
      b_box2(:,:) = dcmplx(0.0,0.0)
      c_box2(:,:) = dcmplx(0.0,0.0)
      d_box2(:,:) = dcmplx(0.0,0.0)

      a_box2_2(:,:) = dcmplx(0.0,0.0)
      b_box2_2(:,:) = dcmplx(0.0,0.0)
      c_box2_2(:,:) = dcmplx(0.0,0.0)
      d_box2_2(:,:) = dcmplx(0.0,0.0)


      call sample_points(P0,w0,L_order(Nmax_box),L_order(Nmax_box))
      allocate(T(size(w0)))
   end if

 
   
   !$omp parallel default(private) &
   !$omp firstprivate(level, Nmax_box, k, diag, Fth, Fph, Gth, Gph, P0, w0) &
   !$omp shared(a_box2, b_box2, c_box2, d_box2) &
   !$omp shared(a_box2_2, b_box2_2, c_box2_2, d_box2_2) &
   !$omp shared(aggregates, aggregates2, otree) 
   !$omp do schedule(dynamic,1) 
   do box = 1, size(otree(level+1)%tree) 
      parent = otree(level+1)%tree(box)%parent ! level - 1 
     
      cp_box =  otree(level+1)%tree(box)%cp ! from

      do i1 = 1, 27 ! near neighbours of parent cube

         neighbour = otree(level)%tree(parent)%near_neighbours(i1)
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
          
            box2 = otree(level)%tree(neighbour)%children(i2) ! level 
            
            if(box2 == 0) exit
          
            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level+1)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do
           
            if(near_box == 0) then
     
               cp_box2 =  otree(level+1)%tree(box2)%cp !to

               if(diag == 0) then
               
                  !**** Translate box to accessible box
                  call translate_xy(cp_box-cp_box2, Nmax_box, Nmax_box, k, &
                       [aggregates(level)%a_out(:,box2),aggregates2(level)%a_out(:,box2)], &
                       [aggregates(level)%b_out(:,box2),aggregates2(level)%b_out(:,box2)], &
                       a_box2_loc, b_box2_loc,1)
                  
            
                  !$omp critical
                  a_box2(:,box) = a_box2(:,box) + a_box2_loc(:,1)
                  b_box2(:,box) = b_box2(:,box) + b_box2_loc(:,1)
                  
                  a_box2_2(:,box) = a_box2_2(:,box) + a_box2_loc(:,2)
                  b_box2_2(:,box) = b_box2_2(:,box) + b_box2_loc(:,2)
                  !$omp end critical
      
                  
               else 
                                  
                  T = diagonal_translator(cp_box-cp_box2, k, Nmax_box, P0, w0)
                  
                  !$omp critical
                  a_box2(:,box) = a_box2(:,box) + conjg(T)*aggregates(level)%a_th(:,box2)
                  b_box2(:,box) = b_box2(:,box) + conjg(T)*aggregates(level)%a_ph(:,box2)
                  c_box2(:,box) = c_box2(:,box) + conjg(T)*aggregates(level)%b_th(:,box2)
                  d_box2(:,box) = d_box2(:,box) + conjg(T)*aggregates(level)%b_ph(:,box2)
                  
                  a_box2_2(:,box) = a_box2_2(:,box) + conjg(T)*aggregates2(level)%a_th(:,box2)
                  b_box2_2(:,box) = b_box2_2(:,box) + conjg(T)*aggregates2(level)%a_ph(:,box2)
                  c_box2_2(:,box) = c_box2_2(:,box) + conjg(T)*aggregates2(level)%b_th(:,box2)
                  d_box2_2(:,box) = d_box2_2(:,box) + conjg(T)*aggregates2(level)%b_ph(:,box2)



                  !$omp end critical
            
                     
               end if
               
            end if
            
         end do
         
      end do
      
      
   end do
   !$omp end do
   !$omp end parallel

   !deallocate(aa_in, ab_in, aa_in2, ab_in2)

   
   if(diag == 0) then
      aggregates(level)%a_out = a_box2
      aggregates(level)%b_out = b_box2

      aggregates2(level)%a_out = a_box2_2
      aggregates2(level)%b_out = b_box2_2

   else

      aggregates(level)%a_th = a_box2
      aggregates(level)%a_ph = b_box2
      aggregates(level)%b_th = c_box2
      aggregates(level)%b_ph = d_box2

      aggregates2(level)%a_th = a_box2_2
      aggregates2(level)%a_ph = b_box2_2
      aggregates2(level)%b_th = c_box2_2
      aggregates2(level)%b_ph = d_box2_2


   end if

   if(diag==1) deallocate(P0, w0, T)
   
    if(diag==0) deallocate(a_box2_loc,b_box2_loc)
   deallocate(a_box2,b_box2, a_box2_2, b_box2_2)
   

   call system_clock(T2)
   if(sphere(1)%ifT == 0) print*, 'Translation done in', real(T2-T1) / real(rate), 'level',level
end do



!***********  Disagregation  *********************************************
! 
! low to high 
!
!*********************************************************************

call system_clock(T1,rate)

if(max_level >= 3) then
do level = 2, max_level-1
   !print*,'fff'
   Nmax_box2 = otree(level+2)%tree(1)%Nmax ! high
   nm_box2 = (Nmax_box2+1)**2-1 ! high
   allocate(a_box_loc(nm_box2,2), b_box_loc(nm_box2,2)) ! high 

   !$omp parallel default(private) &
   !$omp firstprivate(Nmax_box2, k, level) &
   !$omp shared(aggregates,aggregates2, otree)
   !$omp do schedule(dynamic,1)
   do box = 1, size(otree(level+1)%tree) ! low
      Nmax_box1 = otree(level+1)%tree(box)%Nmax ! low
      cp_box = otree(level+1)%tree(box)%cp ! from low

      do i1 = 1, 8
         box2 = otree(level+1)%tree(box)%children(i1) ! high
         if(box2 == 0) exit
         cp_box2 = otree(level+2)%tree(box2)%cp ! to high

         ! translate low to high
         call translate_xy(cp_box2-cp_box, Nmax_box1, Nmax_box2, k, &
            [aggregates(level)%a_out(:,box),aggregates2(level)%a_out(:,box)], &
            [aggregates(level)%b_out(:,box),aggregates2(level)%b_out(:,box)], &
            a_box_loc, b_box_loc,0)
      
         aggregates(level+1)%a_out(:,box2) = & 
               aggregates(level+1)%a_out(:,box2) + a_box_loc(:,1)
         
         aggregates(level+1)%b_out(:,box2) = &
              aggregates(level+1)%b_out(:,box2) + b_box_loc(:,1)
         
         aggregates2(level+1)%a_out(:,box2) = & 
               aggregates2(level+1)%a_out(:,box2) + a_box_loc(:,2)
         
         aggregates2(level+1)%b_out(:,box2) = &
              aggregates2(level+1)%b_out(:,box2) + b_box_loc(:,2)
         
         
      end do
       
   end do
   !$omp end do
   !$omp end parallel

   deallocate(a_box_loc,b_box_loc)
end do
end if

!************   Dissaggregate   ******************


Nmax_box = otree(max_level+1)%tree(1)%Nmax
allocate(a_box_loc((Nmax_box+1)**2-1,2))
allocate(b_box_loc((Nmax_box+1)**2-1,2))

!$omp parallel default(private) &
!$omp firstprivate(k, max_level,Fth, Fph, Gth, Gph, Nmax_box, diag) &
!$omp shared(x_out,x_out2, aggregates, aggregates2, otree, sphere)
!$omp do schedule(dynamic,1)


do box = 1, size(otree(max_level+1)%tree)

    Nmax_box = otree(max_level+1)%tree(box)%Nmax
    cp_box = otree(max_level+1)%tree(box)%cp


    !planewaves to vsws
    if (diag == 0) then
       a_box_loc(:,1) = aggregates(max_level)%a_out(:,box)
       b_box_loc(:,1) = aggregates(max_level)%b_out(:,box)
       a_box_loc(:,2) = aggregates2(max_level)%a_out(:,box)
       b_box_loc(:,2) = aggregates2(max_level)%b_out(:,box)

    else

    

       a_box_loc(:,1) = &
            matmul(transpose(conjg(Fth)),aggregates(max_level)%a_th(:,box)) + & 
            matmul(transpose(conjg(Fph)),aggregates(max_level)%a_ph(:,box)) + & 
            matmul(transpose(conjg(Gth)),aggregates(max_level)%b_th(:,box)) + & 
            matmul(transpose(conjg(Gph)),aggregates(max_level)%b_ph(:,box))

       b_box_loc(:,1) = &
            matmul(transpose(conjg(Fth)),aggregates(max_level)%b_th(:,box)) + & 
            matmul(transpose(conjg(Fph)),aggregates(max_level)%b_ph(:,box)) + & 
            matmul(transpose(conjg(Gth)),aggregates(max_level)%a_th(:,box)) + & 
            matmul(transpose(conjg(Gph)),aggregates(max_level)%a_ph(:,box))


       a_box_loc(:,2) = &
            matmul(transpose(conjg(Fth)),aggregates2(max_level)%a_th(:,box)) + & 
            matmul(transpose(conjg(Fph)),aggregates2(max_level)%a_ph(:,box)) + & 
            matmul(transpose(conjg(Gth)),aggregates2(max_level)%b_th(:,box)) + & 
            matmul(transpose(conjg(Gph)),aggregates2(max_level)%b_ph(:,box))

       b_box_loc(:,2) = &
            matmul(transpose(conjg(Fth)),aggregates2(max_level)%b_th(:,box)) + & 
            matmul(transpose(conjg(Fph)),aggregates2(max_level)%b_ph(:,box)) + & 
            matmul(transpose(conjg(Gth)),aggregates2(max_level)%a_th(:,box)) + & 
            matmul(transpose(conjg(Gph)),aggregates2(max_level)%a_ph(:,box))




    end if



   
   do sph1_loc = 1, otree(max_level+1)%tree(box)%N_source

      sph1 = otree(max_level+1)%tree(box)%sources(sph1_loc)
 
       Nmax = sphere(sph1)%Nmax
       nm = (Nmax+1)**2-1

       i1_a1 = sphere(sph1)%ind_a1
       i1_a2 = sphere(sph1)%ind_a2
       i1_b1 = sphere(sph1)%ind_b1
       i1_b2 = sphere(sph1)%ind_b2
       
                 
       s1 = sphere(sph1)%ind_a2 - sphere(sph1)%ind_a1 + 1           
       allocate(a_out(s1,2), b_out(s1,2))
              
       call translate_xy(sphere(sph1)%cp-cp_box, Nmax_box, Nmax, k, &
            [a_box_loc(:,1),a_box_loc(:,2)], &
            [b_box_loc(:,1),b_box_loc(:,2)],&
            a_out, b_out,0)
      

       x_out(i1_a1:i1_a2) = x_out(i1_a1:i1_a2) + a_out(:,1)
       x_out(i1_b1:i1_b2) =  x_out(i1_b1:i1_b2) + b_out(:,1)

       x_out2(i1_a1:i1_a2) = x_out2(i1_a1:i1_a2) + a_out(:,2)
       x_out2(i1_b1:i1_b2) = x_out2(i1_b1:i1_b2) + b_out(:,2)
       

      !deallocate(a_nm,b_nm,c_nm,d_nm)
      deallocate(a_out, b_out) 

   end do

end do

!$omp end do
!$omp end parallel

call system_clock(T2)
if(sphere(1)%ifT == 0) print*, 'Disaggregation done in', real(T2-T1) / real(rate)

deallocate(aggregates,aggregates2)

end subroutine matvec_mlfmm_diag

!**************************************************************************



!************************************************************************


subroutine matvec_diag(sphere,  k, x, x_out, x2, x_out2)
type (data_struct), dimension(:) :: sphere
complex(dp), dimension(:) :: x , x_out, x2, x_out2
complex(dp) :: k

integer :: sph1, sph2, Nspheres, Nmax, nm, s1, Nmax2, nm2
integer :: i1_a1, i1_a2, i1_b1, i1_b2, i2_a1, i2_a2, i2_b1, i2_b2, box, box2
integer :: sph1_loc, sph2_loc, box2_loc, i1, near_box, Nmax_box
integer :: Nmax_box1, Nmax_box2, L_max_box
complex(dp), dimension(:), allocatable :: a_nm, b_nm, c_nm, d_nm, T
complex(dp), dimension(:,:), allocatable :: a_loc, b_loc, a_out, b_out
complex(dp), dimension(:,:), allocatable :: aa_in, ab_in, aa_in2, ab_in2
integer :: nm_box, nm_box2, max_level, level_ind, level, parent, neighbour, i2, i3
integer :: T1, T2, rate
integer :: mem, diag, nm_box_pw


Nspheres = size(sphere)


 
!*************** Near-zone interactions *******************************

!$omp parallel default(private) &
!$omp firstprivate(k, level_ind, Nspheres) &
!$omp shared(x_out,x_out2,x,x2, sphere) 
!$omp do schedule(dynamic,1)

do sph1 = 1, Nspheres
 
   Nmax = sphere(sph1)%Nmax
   nm = (Nmax+1)**2-1
 
   s1 = sphere(sph1)%ind_a2 - sphere(sph1)%ind_a1 + 1
   allocate(a_loc(s1,2), b_loc(s1,2))
   allocate(a_out(s1,2), b_out(s1,2))
   a_loc(:,:) = dcmplx(0.0,0.0)
   b_loc(:,:) = dcmplx(0.0,0.0)

   i1_a1 = sphere(sph1)%ind_a1
   i1_a2 = sphere(sph1)%ind_a2
   i1_b1 = sphere(sph1)%ind_b1
   i1_b2 = sphere(sph1)%ind_b2
   

   do sph2 = 1, Nspheres

         
      i2_a1 = sphere(sph2)%ind_a1
      i2_a2 = sphere(sph2)%ind_a2
      i2_b1 = sphere(sph2)%ind_b1
      i2_b2 = sphere(sph2)%ind_b2

      Nmax2 = sphere(sph2)%Nmax
      nm2 = (Nmax2+1)**2-1

      if(sph1 .ne. sph2) then
          

         call translate_xy(sphere(sph1)%cp-sphere(sph2)%cp, Nmax2, Nmax,&
              k, [x(i2_a1:i2_a2),x2(i2_a1:i2_a2)], &
              [x(i2_b1:i2_b2),x2(i2_b1:i2_b2)], a_out, b_out,1)
         

         a_loc(:,1) = a_loc(:,1) + a_out(:,1)  
         b_loc(:,1) = b_loc(:,1) + b_out(:,1)
         
         a_loc(:,2) = a_loc(:,2) + a_out(:,2)  
         b_loc(:,2) = b_loc(:,2) + b_out(:,2)

      end if
            
   end do

   x_out(i1_a1:i1_a2) = a_loc(:,1)
   x_out(i1_b1:i1_b2) = b_loc(:,1)

   x_out2(i1_a1:i1_a2) = a_loc(:,2)
   x_out2(i1_b1:i1_b2) = b_loc(:,2)


   deallocate(a_out, b_out)
   deallocate(a_loc, b_loc)
    
 
   
end do

!$omp end do
!$omp end parallel

end subroutine matvec_diag


end module matvec

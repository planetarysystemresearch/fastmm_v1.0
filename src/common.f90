module common 

implicit none 

integer, parameter :: dp = selected_real_kind(15, 307)
real(dp), parameter :: pi = 4.0_dp*atan(1.0_dp)
real(dp), parameter :: epsilon = 8.854187817*(10**(-12.0))
real(dp), parameter :: mu = 4.0*pi*10.0**(-7.0)


type data_struct
   integer :: Nmax, ind_a1, ind_a2, ind_b1, ind_b2
   integer :: Tmat_ind, ifT
   real(dp) :: euler_angles(3)
   real(dp) :: cp(3), r
   complex(dp) :: eps_r
   complex(dp), dimension(:,:), allocatable :: A, B
end type data_struct

type Tmatrix
   complex(dp), dimension(:,:), allocatable :: Taa, Tbb, Tab, Tba
end type Tmatrix

contains

function factorial(a) result(c)
integer :: i1, a
real(dp) :: c, b

b=1.0
do i1 = 1,a
   b = b * i1
end do
c=dble(b)

end function factorial


function cart2sph(x) result(vec)
real(dp), intent(in) :: x(3)
real(dp) :: vec(3)

vec(1) = sqrt(x(1)**2 + x(2)**2 + x(3)**2) ! r
vec(2) = acos(x(3)/vec(1)) ! theta
vec(3) = atan2(x(2),x(1))   

end function cart2sph

function cart2sph2(x) result(vec)
real(dp), intent(in) :: x(3)
real(dp) :: vec(3)

vec(1) = sqrt(x(1)**2 + x(2)**2 + x(3)**2) ! r
vec(2) = atan2(sqrt(x(1)**2+x(2)**2),x(3))
vec(3) = atan2(x(2),x(1))   

end function cart2sph2


function sph2cart(r,theta,phi) result(x)
real(dp), intent(in) :: r, theta, phi
real(dp) :: x(3)

x(1) = r*sin(theta)*cos(phi)
x(2) = r*sin(theta)*sin(phi)
x(3) = r*cos(theta)

end function sph2cart

function sph2cart_vec(theta, phi, vec) result(vec2)
complex(dp), intent(in) :: vec(3)
real(dp), intent(in) :: theta, phi
complex(dp) :: vec2(3)
real(dp) :: H(3,3)

H(1,:) = [sin(theta)*cos(phi), cos(theta)*cos(phi), -sin(phi)]
H(2,:) = [sin(theta)*sin(phi), cos(theta)*sin(phi), cos(phi)]
H(3,:) = [cos(theta), -sin(theta), dble(0.0)];

vec2 = matmul(H,vec)

end function sph2cart_vec

function norm(r, rp) result(c)
real(dp), dimension(3), intent(in) :: r, rp
real(dp) :: c
c = sqrt((r(1)-rp(1))**2 + (r(2)-rp(2))**2 + (r(3)-rp(3))**2)
end function norm

function binomial(n,k) result(c)
integer :: n, k, i1
real(dp) :: c

c = 1.0d0
do i1 = 1,k
   c = c * (n + 1.0d0 - i1)/i1
end do

end function binomial


function rotation_angles(x) result(vec)
real(dp), intent(in) :: x(3)
real(dp) :: vec(3)

vec(1) = sqrt(x(1)**2 + x(2)**2 + x(3)**2) 
vec(3) = atan2(x(2),x(1))     ! phi      
vec(2) = atan2(x(1),x(3))   

end function rotation_angles

function rotation_matrix(a,b,g) result(rot)
real(dp) :: a, b, g
real(dp) :: rot(3,3)

rot(1,1) = cos(b)*cos(g);
rot(1,2) = cos(a)*sin(g) + sin(a)*sin(b)*cos(g);
rot(1,3) = sin(a)*sin(g) - cos(a)*sin(b)*cos(g);

rot(2,1) = -cos(b)*sin(g);
rot(2,2) = cos(a)*cos(g) - sin(a)*sin(b)*sin(g);
rot(2,3) = sin(a)*cos(g) + cos(a)*sin(b)*sin(g);

rot(3,1) = sin(b);
rot(3,2) = -sin(a)*cos(b);
rot(3,3) = cos(a)*cos(b);

end function rotation_matrix

function rotation_matrix_euler(a,b,g) result(rot)
real(dp) :: a, b, g
real(dp) :: rot(3,3)

rot(1,1) = cos(a)*cos(b);
rot(1,2) = cos(a)*sin(b)*sin(g) - sin(a)*cos(g);
rot(1,3) = cos(a)*sin(b)*cos(g) + sin(a)*sin(g);

rot(2,1) = sin(a)*cos(b);
rot(2,2) = sin(a)*sin(b)*sin(g) + cos(a)*cos(g);
rot(2,3) = sin(a)*sin(b)*cos(g) - cos(a)*sin(g);

rot(3,1) = -sin(b);
rot(3,2) = cos(b)*sin(g);
rot(3,3) = cos(b)*cos(g);

end function rotation_matrix_euler

function crossCC(a,b) result(c)
complex(dp), intent(in) :: a(3), b(3)
complex(dp) :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossCC

function truncation_order(ka) result(Nmax)
real(dp) :: ka 
integer :: Nmax

if(ka > 1.0d0) then
   Nmax = floor(ka + 3.0* (ka)**(1.0/3.0))
else  
   
   if(ka<2.0d0) Nmax = 4
   if(ka<0.1d0) Nmax = 3
   if(ka<0.01d0) Nmax = 2
   if(ka<0.0001d0) Nmax = 1
end if

end function truncation_order

function truncation_order2(ka) result(Nmax)
real(dp) :: ka 
integer :: Nmax
real(dp) :: err, sigma, ph, pl

sigma = 2
err = 0.001

ph = (ka + 1.0/2.0 * (3.0*log(1/err))**(2.0/3.0)*  (ka)**(1.0/3.0)) 
pl = 1/log(sigma) * log(1.0/(err*(1.0-1.0/sigma)**(3.0/2.0))) + 1
Nmax = ceiling((pl**4.0 + ph**4.0)**(1.0/4.0))

end function truncation_order2

function halton_seq(index, base) result(num)
integer :: base, index, i
real(dp) :: num, f
num = 0.0

f = 1.0/dble(base)

i = index

do while(i>0) 
   num = num + f * mod(i,base)
   i = floor(dble(i)/dble(base))
   f = f / dble(base)
end do

end function halton_seq


end module common 

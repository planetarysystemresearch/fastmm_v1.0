!-------------------------------------------------------------------
!Fast superposition T-matrix Method (FaSTMM)
!The main program
!
!Last modified 15.4.2018
!
!The new BSD License is applied to this software, see LICENSE.txt
!Copyright (C) 2016 Johannes Markkanen, University of Helsinki
!All rights reserved.
!
!-----------------------------

program main 
use sfunctions
use mie
use common
use translations
use io
use solver 
use geometry
use octtree
use T_matrix
use integration_points
use orientation_averaging
implicit none

integer :: nm ,i1, Nspheres, sph, Nmax, max_sph
complex(dp), dimension(:), allocatable :: xx, xx2, b_vec, a_in, b_in, ax, b_vec2
complex(dp), dimension(:), allocatable :: a_nm, b_nm, param, a_nm2, b_nm2
complex(dp), dimension(:), allocatable :: a_nm_in, b_nm_in, & 
     a_nm_in2, b_nm_in2, c_nm, d_nm, ta, tb
real(dp), dimension(:), allocatable :: radius
integer, dimension(:), allocatable :: tindex
complex(dp), dimension(:,:), allocatable :: ab, E_out, E_out2, EE, jones
real(dp), dimension(:,:), allocatable :: coord, mueller, angles, Cexts
real(dp) :: ka, crs(4)
complex(dp) :: k

type (data_struct), dimension(:), allocatable :: sphere
type (level_struct), dimension(:), allocatable :: otree
type (Tmatrix), dimension(:), allocatable :: Tmat

real(dp) ::  maxrad, maxrad_sph, orig(3), rotmat(3,3), rad, crs5(5)
character(len=38) :: Eout, geo_read, EE_out, S_out, T_out, mueller_out2

character(len=38) :: arg_name, arg, ab_out, Tmat_file, rotmat_file, J_out 
real(dp) :: k_in, packdens, eps_r, eps_i, vol, absb
integer :: i_arg, num_args, geo, max_level, T1, T2, rate, las, n, nm_in
integer :: N_phi, N_theta, N_tmat, T_Nmax, restart, max_iter, ifT, N_ave
real(dp) :: Cext, Cabs, Csca, Qext, Qsca, Qabs, tol
real(dp) :: Cext2, Cabs2, Csca2, maxcc(3), mincc(3), cc(3)
complex(dp), dimension(:,:), allocatable :: TT
complex(dp), dimension(:,:,:), allocatable :: Taa, Tab, Tba, Tbb
complex(dp), dimension(:,:), allocatable :: Taa2, Tab2, Tba2, Tbb2
complex(dp), dimension(:), allocatable :: rotD
complex(dp), dimension(:), allocatable :: a_in2, b_in2
real(dp), dimension(:,:), allocatable :: SS, S_ave
integer, dimension(:,:), allocatable :: indD

integer :: Nthreads, T1tot, T2tot, ratetot
real(dp), allocatable :: P0(:,:), w0(:)
complex(dp), allocatable :: T(:), Fth(:,:), Fph(:,:), Gth(:,:), Gph(:,:)
integer :: max_loc(3), min_loc(3), fabs, max_ti, halton_init


call system_clock(T1tot,ratetot)

print*, '*********************************************************' 
print*, '*                                                       *' 
print*, '*                   FaSTMM v 1.01                        *' 
print*, '*                                                       *' 
print*, '*********************************************************' 

!$omp parallel default(shared) 
Nthreads = omp_get_num_threads()
!$omp end parallel

print*, 'Number of threads =',Nthreads 


! Input parameters 

ab_out = 'ab.h5'                 ! Output file for the coefficients
Nspheres = 100                   ! Number of spheres (if geo = 2)
packdens = 0.25                  ! Packing density (if geo = 2) 
eps_r = 3.0                    
eps_i = 1.0
geo_read = 'geometry.h5'         ! Read geometry from the file
geo = 1
EE_out = 'E.h5'                  ! Output file for electric fields
S_out = 'mueller.h5'             ! Output file for Mueller matrix 
T_out = 'Tout.h5'
J_out = 'jones.h5'
N_phi = 32                           
N_theta = 181
k_in = 1.0                       ! Wavenumber 
N_tmat = 0
Tmat_file = 'T.h5'
restart = 5
max_iter = 50
tol = 0.0001
ifT = 0
rad = 1.0
N_ave = 0
halton_init = 0
! Read command line arguments 
num_args = command_argument_count()
do i_arg = 1,num_args,2
   call get_command_argument(i_arg,arg_name)
   
   select case(arg_name)
      
   case('-geometry_file') 
      call get_command_argument(i_arg+1,arg)
      geo_read = arg
   case('-Tmat_file') 
      call get_command_argument(i_arg+1,arg)
      Tmat_file = arg
   case('-Tmatrix')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) ifT
   case('-T_out') 
      call get_command_argument(i_arg+1,arg)
      T_out = arg
   case('-geometry')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) geo
   case('-k')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) k_in 
    case('-radius')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) rad 
    case('-N_spheres')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) Nspheres 
    case('-density')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) packdens 
  case('-eps_r')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) eps_r
    case('-eps_i')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) eps_i 
   case('-coeff_out')
      call get_command_argument(i_arg+1,arg)
      ab_out = arg
   case('-E_out')
      call get_command_argument(i_arg+1,arg)
      EE_out = arg
   case('-S_out')
      call get_command_argument(i_arg+1,arg)
      S_out = arg
   case('-J_out')
      call get_command_argument(i_arg+1,arg)
      J_out = arg
   case('-N_ave')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) N_ave
   case('-halton_init')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) halton_init   
   case('-N_phi')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) N_phi 
   case('-N_theta')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) N_theta 
   case('-N_Tmat')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) N_tmat
   case('-restart')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) restart
   case('-max_iter')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) max_iter
   case('-tol')
      call get_command_argument(i_arg+1,arg)
      read(arg,*) tol
   case default 
      print '(a,a,/)', 'Unrecognized command-line option: ', arg_name
      stop
   end select
end do

k = dcmplx(k_in,0.0)

!*********************************************************************
!nmax = 5
!call sample_points(P0,w0,4*nmax+1,4*nmax+1)
!allocate(T(size(w0)))
!T = diagonal_translator([0.0d0,5.0d0,5.0d0], k, nmax, P0, w0)

!allocate(Fth(size(w0),(nmax+1)**2-1))
!allocate(Fph(size(w0),(nmax+1)**2-1))
!allocate(Gth(size(w0),(nmax+1)**2-1))
!allocate(Gph(size(w0),(nmax+1)**2-1))

!print*, size(Fth,1), size(Fth,2)
!call sph2vswf(P0, nmax, k, Fth, Fph, Gth, Gph)

!stop

allocate(Tmat(N_tmat))
! Read T-matric for inclusions 
if(N_tmat .ne. 0) then
   !call read_T(Tmat_file, Taa, Tab, Tba, Tbb)
   call read_T2(Tmat_file, Taa, Tab, Tba, Tbb)

   do i1 = 1, N_tmat
     
      Tmat(i1)%Taa = Taa(:,:,i1)
      Tmat(i1)%Tab = Tab(:,:,i1)
      Tmat(i1)%Tba = Tba(:,:,i1)
      Tmat(i1)%Tbb = Tbb(:,:,i1)
   end do
   print*, 'Number of T-matrices read =', size(Tmat)
end if 


!*********    geometry reader ***********************************
! read goemetry from file
if(geo == 1) then

   call read_geometry(geo_read, coord, radius, param, angles, tindex)
   Nspheres = size(radius)
   if(maxval(tindex)> N_tmat .and. N_tmat .ne. 0) then
      print*, 'Not enought T-matrices stored in ', Tmat_file
      stop
   end if
end if

! Build spherical random medium
if(geo == 2) then 
   allocate(coord(3,Nspheres))
   allocate(radius(Nspheres))
   allocate(param(Nspheres))
   allocate(angles(3,Nspheres))
    allocate(tindex(Nspheres))
    call pack_spheres(Nspheres, packdens, rad, dcmplx(eps_r,eps_i), N_tmat, &
         coord,radius,param, angles, tindex)

    !call pack_cylinder(Nspheres, packdens, rad, dcmplx(eps_r,eps_i), N_tmat, &
    !     coord,radius,param, angles, tindex)
    
end if

max_loc(1) =  maxloc(coord(1,:),1)
max_loc(2) = maxloc(coord(2,:),1)
max_loc(3) = maxloc(coord(3,:),1)

!print*, max_loc

min_loc(1) = minloc(coord(1,:),1)
min_loc(2) = minloc(coord(2,:),1)
min_loc(3) = minloc(coord(3,:),1)

cc(1) = ((coord(1,max_loc(1)) + radius(max_loc(1))) + &
     (coord(1,min_loc(1)) - radius(min_loc(1))))/2.0d0
cc(2) = ((coord(2,max_loc(2)) + radius(max_loc(2))) + &
     (coord(2,min_loc(2)) - radius(min_loc(2))))/2.0d0
cc(3) = ((coord(3,max_loc(3)) + radius(max_loc(3))) + &
     (coord(3,min_loc(3)) - radius(min_loc(3))))/2.0d0

!print*, cc
do i1 = 1, Nspheres
   coord(:,i1) = coord(:,i1) - cc
end do

allocate(sphere(Nspheres))
!allocate(Tmat(N_tmat))

! Read T-matric for inclusions 
!if(N_tmat .ne. 0) then
!   !call read_T(Tmat_file, Taa, Tab, Tba, Tbb)
!   call read_T2(Tmat_file, Taa, Tab, Tba, Tbb)

!   do i1 = 1, N_tmat
!      print*, i1
!      Tmat(i1)%Taa = Taa(:,:,i1)
!      Tmat(i1)%Tab = Tab(:,:,i1)
!      Tmat(i1)%Tba = Tba(:,:,i1)
!      Tmat(i1)%Tbb = Tbb(:,:,i1)
!   end do
!end if 


print*, 'Wavenumber:', k_in
print*, 'Number of spheres:', Nspheres
print*, 'Packing density:', packdens
print*, 'Mueller matrix is written in the file:', S_out


!*************** 
maxrad = 0.0d0
max_sph = 0
vol = 0.0d0

do sph = 1, Nspheres
    
   sphere(sph)%cp = coord(:,sph)
   sphere(sph)%r = radius(sph)
   sphere(sph)%eps_r = param(sph)
   
   ka = real(k) * radius(sph)
   sphere(sph)%Nmax = truncation_order(ka)  ! Note

   sphere(sph)%Tmat_ind = tindex(sph)
  


   if(N_tmat .ne. 0) then
      sphere(sph)%euler_angles = angles(:,sph)
      
      T_Nmax = sqrt(size(Tmat(sphere(sph)%Tmat_ind)%Taa,1)+1.0) - 1
      !print*, 'sphere Nmax=', T_Nmax
      sphere(sph)%Nmax = T_Nmax
      if(T_Nmax .ne. sphere(sph)%Nmax) then
         print*, T_Nmax, sphere(sph)%Nmax
         print*, 'T-matrix size does not match' 
         stop
      end if
   end if

   maxrad_sph = sqrt(dot_product(coord(:,sph), coord(:,sph)))  + radius(sph)
   if(maxrad < maxrad_sph) then
      maxrad = maxrad_sph
   end if

   if(max_sph < sphere(sph)%Nmax) then
      max_sph = sphere(sph)%Nmax
   end if

   vol = vol + 4.0/3.0*pi*(radius(sph))**3

end do


!********************************************************************
if(N_ave > 0) then
   call orientation_ave(sphere, Tmat, ifT,  k, N_phi, N_theta, N_ave, halton_init, S_ave, crs5)
  
   call write2file_mueller(S_ave, crs5, S_out)

else
!************************************************
print*, 'build octree...' 
call create_octtree(sphere, otree, dble(k), max_level)

do i1 = 1,size(otree)
   print*, 'level',i1-1,size(otree(i1)%tree), ', dl=',real(otree(i1)%tree(1)%dl), 'Nmax', otree(i1)%tree(1)%Nmax
end do

ka = dble(k)*sqrt(3.0d0)/2.0d0 * otree(1)%tree(1)%dl
Nmax = truncation_order(ka) 

sphere(1)%ifT = ifT

print*, 'max order', Nmax
PRINT*, 'max sphere order', max_sph
print*, 'volume equivalent size ', (3.0*vol/(4.0*pi)) ** (1.0/3.0) * dble(k) 



!******* exact Solver ***********************************************


print*, 'compute rhs...' 
call system_clock(T1,rate)

call rhs2_xy(Nmax, sphere, Tmat, b_vec, b_vec2, k)

call system_clock(T2)
print*, 'rhs done in ', real(T2-T1) / real(rate)

allocate(xx(size(b_vec)))
allocate(xx2(size(b_vec)))

print*, 'Estimated memory usage', 2*max_iter*sizeof(xx)/1024/1024,'MB' 

call gmres_mlfmm2(sphere, otree, Tmat, k, b_vec,b_vec2, xx, xx2, tol, restart, max_iter)

!xprint*, xx

print*, 'Post processing...'
call system_clock(T1,rate)
deallocate(b_vec,b_vec2)
call inc_xy(Nmax, sphere, b_vec, b_vec2, k)
!call compute_cluster_fields(sphere, xx, b_vec, k, N_theta, N_phi , E_out)
!call compute_cluster_fields(sphere, xx2, b_vec2, k, N_theta, N_phi, E_out2)


!*************************************************************

fabs = 1

if(fabs == 1) then

   ! Mueller matrix from sphere coefficients (computes absoption 
   ! coefficient by integrating the internal fields of spheres)

   call mueller_matrix(sphere, xx, xx2, b_vec, b_vec2, k, N_theta, N_phi, Nmax, mueller, jones, crs)

else

   ! compute cluster coefficients / Mueller matrix from cluster coefficients
   ! (computes absorption coefficient from the optical theorem Cabs=Cext-Csca)
   ! Not as accurate as integrating the internal fields but faster
 
   call ml_cluster_coeff2(sphere, otree, xx,xx2, k, a_nm, b_nm, a_nm2, b_nm2)

   call mueller_matrix_coeff(a_nm, b_nm, a_nm2, b_nm2, k, N_theta, N_phi, Nmax, mueller, jones)

   allocate(a_in((Nmax+1)**2-1))
   allocate(b_in((Nmax+1)**2-1))
   allocate(a_in2((Nmax+1)**2-1))
   allocate(b_in2((Nmax+1)**2-1))

   
   call planewave2(Nmax, k, a_in, b_in, a_in2, b_in2)
    
   call cross_sections(a_nm, b_nm, a_in, b_in, k, Nmax, Cext, Csca, Cabs)
   print*, 'Cext_x', Cext
   print*, 'Csca_x', Csca
   print*, 'Cabs_x', Cabs
   call cross_sections(a_nm2, b_nm2, a_in2, b_in2, k, Nmax, Cext2, Csca2, Cabs2)
   print*, 'Cext_y', Cext2
   print*, 'Csca_y', Csca2
   print*, 'Cabs_y', Cabs2
   
   crs(1) = (Cext + Cext2) / 2.0d0
   crs(2) = (Csca + Csca2) / 2.0d0
   crs(3) = (Cabs + Cabs2) / 2.0d0

end if

!**************************************************************

call system_clock(T2)
print*, 'Done in ', real(T2-T1) / real(rate)
 

call write2file_mueller(mueller, crs, S_out)
call write2file(jones,J_out)

end if

if(sphere(1)%ifT == 1) then 

   allocate(Taa2((Nmax + 1)**2-1, (Nmax + 1)**2-1))
   allocate(Tbb2((Nmax + 1)**2-1, (Nmax + 1)**2-1))
   allocate(Tab2((Nmax + 1)**2-1, (Nmax + 1)**2-1))
   allocate(Tba2((Nmax + 1)**2-1, (Nmax + 1)**2-1))

   call compute_T_matrix(sphere, otree, Tmat, k, Nmax, tol, restart, &
        max_iter, Taa2, Tab2, Tba2, Tbb2)

    call T_write2file(Taa2, Tab2, Tba2, Tbb2, T_out)

   allocate(a_in((Nmax+1)**2-1))
   allocate(b_in((Nmax+1)**2-1))
   call planewave(Nmax, k, a_in, b_in)

   las = 0
   do n = 1,Nmax
      las = las + (2*n+1)**2
   end do

   allocate(rotD(las))
   allocate(indD(las,2))

   call sph_rotation_sparse(0.0d0, -pi/2.0d0, Nmax, rotD, indD)
   a_in2 = sparse_matmul(rotD,indD,a_in,(Nmax+1)**2-1)
   b_in2 = sparse_matmul(rotD,indD,b_in,(Nmax+1)**2-1)
   
   a_nm = matmul(Taa2,a_in) + matmul(Tab2,b_in)
   b_nm = matmul(Tbb2,b_in) + matmul(Tba2,a_in)
   
   a_nm2 = matmul(Taa2,a_in2) + matmul(Tab2,b_in2)
   b_nm2 = matmul(Tbb2,b_in2) + matmul(Tba2,a_in2)

   deallocate(jones)
   call mueller_matrix_coeff(a_nm, b_nm, a_nm2, b_nm2, dcmplx(k), 180, 1, Nmax, SS, jones)
   mueller_out2 = 'mueller2.h5'

   call cross_sections(a_nm, b_nm, a_in, b_in, k, Nmax, Cext, Csca, Cabs)
   print*, Cext
   print*, Csca
   print*, Cabs

   call tr_T(Taa2, Tbb2, Tab2, Tba2, real(k), crs)

   call write2file_mueller(SS, crs, S_out)
   !call real_write2file(SS,mueller_out2)
end if

!allocate(EE(6,size(E_out,2)))
!EE(1:3,:) = E_out
!EE(4:6,:) = E_out2
!call write2file(EE,EE_out)

call system_clock(T2tot)
print*, 'Wall time:', real(T2tot-T1tot) / real(ratetot),'s'
 
end program

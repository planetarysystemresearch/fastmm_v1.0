module mie

use sfunctions
use common
use omp_lib
use translations

implicit none

contains

subroutine cluster_out2in(sphere, k, x)
  type (data_struct), dimension(:) :: sphere
  complex(dp), dimension(:) :: x
  integer :: las, a1 ,a2, b1,b2, Nmax, m, n, sph
  real(dp) :: cp(3), rad
  complex(dp), allocatable :: a_n(:), b_n(:), c_n(:), d_n(:)
  complex(dp) :: epsr, cn, dn, k
  
  do sph = 1, size(sphere)
     
     a1 = sphere(sph)%ind_a1
     a2 = sphere(sph)%ind_a2
     b1 = sphere(sph)%ind_b1
     b2 = sphere(sph)%ind_b2
     cp = sphere(sph)%cp
     rad = sphere(sph)%r
     epsr = sphere(sph)%eps_r
     
     Nmax = sphere(sph)%Nmax


     allocate(a_n((Nmax+1)**2-1))
     allocate(b_n((Nmax+1)**2-1))
     allocate(c_n((Nmax+1)**2-1))
     allocate(d_n((Nmax+1)**2-1))
     
     call mie_coeff_nm(Nmax, real(k)*rad, sqrt(epsr), a_n, b_n, c_n, d_n)
     
    
     las = 0
     do n = 1, Nmax

        do m = -n, n

           !cn = -(1.0_dp / c_n(las) + 1.0_dp) * x(a1+las)
           !dn = -(1.0_dp / d_n(las) + 1.0_dp) * x(b1+las)


           cn =  c_n(las)/a_n(las) *  x(a1+las)
           dn =  d_n(las)/b_n(las) *  x(b1+las)
           
           x(a1+las) = dn
           x(b1+las) = cn
           las = las + 1

           
        end do
     end do

     deallocate(a_n,b_n,c_n,d_n)
  end do
   
end subroutine cluster_out2in



subroutine cluster_in2out(sphere, k, x)
  type (data_struct), dimension(:) :: sphere
  complex(dp), dimension(:) :: x
  integer :: las, a1 ,a2, b1,b2, Nmax, m, n, sph
  real(dp) :: cp(3), rad
  complex(dp), allocatable :: a_n(:), b_n(:), c_n(:), d_n(:)
  complex(dp) :: epsr, cn, dn, k
  
  do sph = 1, size(sphere)
     
     a1 = sphere(sph)%ind_a1
     a2 = sphere(sph)%ind_a2
     b1 = sphere(sph)%ind_b1
     b2 = sphere(sph)%ind_b2
     cp = sphere(sph)%cp
     rad = sphere(sph)%r
     epsr = sphere(sph)%eps_r
     
     Nmax = sphere(sph)%Nmax


     allocate(a_n((Nmax+1)**2-1))
     allocate(b_n((Nmax+1)**2-1))
     allocate(c_n((Nmax+1)**2-1))
     allocate(d_n((Nmax+1)**2-1))
     
     call mie_coeff_nm(Nmax, real(k)*rad, sqrt(epsr), a_n, b_n, c_n, d_n)
     
        
     las = 0
     do n = 1, Nmax

        do m = -n, n

           !cn = -(1.0_dp / c_n(las) + 1.0_dp) * x(a1+las)
           !dn = -(1.0_dp / d_n(las) + 1.0_dp) * x(b1+las)


           cn =  a_n(las)/c_n(las) *  x(a1+las)
           dn =  b_n(las)/d_n(las) *  x(b1+las)
           
           x(a1+las) = cn
           x(b1+las) = dn
           las = las + 1

           
        end do
     end do

     deallocate(a_n,b_n,c_n,d_n)
  end do
   
end subroutine cluster_in2out
  

! ***************************************************************
!
! Routine computes the absorption cross section by integrating the 
! internal fields of a sphere
!
!****************************************************************
subroutine sphere_absorbtion2(a_in, b_in, k, a, epsr, Nmax, absb)
complex(dp), dimension(:) :: a_in, b_in
real(dp) :: k, a, absb
integer :: Nmax
complex(dp) :: epsr

complex(dp), dimension((Nmax+1)**2-1) ::a_n, b_n, c_n, d_n
complex(dp) :: mx, dn, cn
real(dp) ::  int, x
integer :: n, m, las, i2

x = k*a

call mie_coeff_nm(Nmax, x, sqrt(epsr), a_n, b_n, c_n, d_n)

las = 1
int = 0.0
do n = 1, Nmax

   do m = -n, n

      cn = -dble(1.0d0 / a_n(las) + 1.0d0)
      dn = -dble(1.0d0 / b_n(las) + 1.0d0)

      int = int + 1/k**2 * (dn*abs(a_in(las))**2 + cn*abs(b_in(las))**2)
      
      las = las + 1
   end do

end do

absb =  int 

end subroutine sphere_absorbtion2

!******************************************************************
! 
! Routine computes the Scattering and extinction cross sections
!
!******************************************************************
subroutine cross_sections(a_out, b_out, a_in, b_in, k, Nmax, Cext, Csca, Cabs)
real(dp) :: Cext, Cabs, Csca
complex(dp) :: k
complex(dp), dimension(:) :: a_out, a_in, b_out, b_in
integer :: las, n, m, Nmax
real(dp) :: ee

Cext=0.0
Csca=0.0
las = 1
do n = 1, Nmax
   do m = -n,n
     !ee = n*(n+1) / (2*n+1) * factorial(n+m) / factorial(n-m)
     Csca = Csca + dble(1/k**2 * (a_out(las)*conjg(a_out(las)) + b_out(las)*conjg(b_out(las))))
     Cext = Cext - real(1/k**2 * (a_out(las)*conjg(a_in(las)) + b_out(las)*conjg(b_in(las))))

     las = las + 1
   end do
end do

Cabs = Cext - Csca

end subroutine cross_sections

!********************************************************************
!
! Bohren Huffman mie coefficients
!
!********************************************************************
subroutine bhmie(NSTOP, X,REFREL,a_n,b_n)  
real(dp) :: X
complex(dp) :: REFREL 
INTEGER :: J,JJ,N,NSTOP,NMX,NN
real(dp) :: CHI,CHI0,CHI1,DANG,DX,EN,FN,P,PII,PSI,PSI0,PSI1, THETA,XSTOP,YMOD
 
complex(dp) :: AN,AN1,BN,BN1,DREFRL,XI,XI1,Y
complex(dp) :: D(NSTOP+1), a_n(NSTOP), b_n(NSTOP)


!C*** Obtain pi:
PII=4.*ATAN(1.D0)
DX=X
DREFRL=REFREL
Y=X*DREFRL
YMOD=ABS(Y)
!
!*** Series expansion terminated after NSTOP terms
!    Logarithmic derivatives calculated from NMX on down
     
NMX=NSTOP
   
!*** Logarithmic derivative D(J) calculated by downward recurrence
!    beginning with initial value (0.,0.) at J=NMX
!
D(NMX)=(0.0d0,0.0d0)
NN=NMX-1
DO  N=1,NN
   EN=NMX-N+1
   D(NMX-N)=(EN/Y)-(1./(D(NMX-N+1)+EN/Y))
end do
!
!*** Riccati-Bessel functions with real argument X
!    calculated by upward recurrence
!
PSI0=COS(DX)
PSI1=SIN(DX)
CHI0=-SIN(DX)
CHI1=COS(DX)
XI1=DCMPLX(PSI1,-CHI1)

P=-1.
DO N=1,NSTOP
   EN=N
   FN=(2.0d0*EN+1.0d0)/(EN*(EN+1.0d0))
   
   PSI=(2.0d0*EN-1.0d0)*PSI1/DX-PSI0
   CHI=(2.0d0*EN-1.0d0)*CHI1/DX-CHI0
   XI=DCMPLX(PSI,-CHI)
   
   IF(N.GT.1)THEN
      AN1=AN
      BN1=BN
      a_n(N-1) = -an1
      b_n(N-1) = -bn1
      
   ENDIF
   
   AN=(D(N)/DREFRL+EN/DX)*PSI-PSI1
   AN=AN/((D(N)/DREFRL+EN/DX)*XI-XI1)
   BN=(DREFRL*D(N)+EN/DX)*PSI-PSI1
   BN=BN/((DREFRL*D(N)+EN/DX)*XI-XI1)
   
   PSI0=PSI1
   PSI1=PSI
   CHI0=CHI1
   CHI1=CHI
   XI1=DCMPLX(PSI1,-CHI1)
   
END DO

end subroutine bhmie




! INPUT: xo - size parameter, msph - sphere refractive index (complex)
! mmed - medium refractive index (complex) nang - number of angles
subroutine absMie(xo, msph, mmed, nstop, an, bn, cn, dn) 

  REAL(KIND=dp), INTENT(IN) :: xo
  COMPLEX(KIND=dp), INTENT(IN) :: msph, mmed
  INTEGER :: nstop, ymod, nmx, i, j
  REAL(KIND=dp) :: A, xi2, xi2e, ax, ay, theta, xt
  REAL(KIND=dp), DIMENSION(:), ALLOCATABLE :: p, t
  COMPLEX(KIND=dp) :: mr, k, k2, x, y, ct, ct2, ct3, ct4, ct5
  COMPLEX(KIND=dp), DIMENSION(:), ALLOCATABLE :: psi, psid, psi2, psi2d, &
    xi, xid
   COMPLEX(KIND=dp), DIMENSION(nstop) :: an, bn, cn, dn

  nstop = nstop-1 
  mr = msph/mmed
  k = mmed
  k2 = msph
  x = k*xo
  y = k2*xo
  ymod = INT(ABS(y))
  nmx = MAX(nstop, ymod) + 15
  IF(AIMAG(x) /= 0) THEN
    xi2 = 2.0_dp * AIMAG(x)
    xi2e = EXP(xi2)
    A = REAL(k,dp)*(xi2e/xi2 + (1.0_dp-xi2e) / xi2**2)
  ELSE
    A = 0.5_dp * INT(k,dp)
  END IF

  ALLOCATE(psi(0:nstop), psid(0:nstop), psi2(0:nstop), psi2d(0:nstop), &
    xi(0:nstop), xid(0:nstop), &
    p(0:nstop), t(0:nstop))
  CALL rbj(x, psi, psid)
  CALL rbj(y, psi2, psi2d)
  CALL rby(x, xi, xid)
  xi(:) = psi(:) + (0.0_dp, 1.0_dp) * xi(:)
  xid(:) = psid(:) + (0.0_dp, 1.0_dp) * xid(:)
  
  DO i=1,nstop
    ct = psi(i)*psi2d(i)
    ct2 = psi2(i)*psid(i)
    ct3 = mr*(psi(i)*xid(i) - xi(i)*psid(i))
    ct4 = (mr*psi2(i)*xid(i) - xi(i)*psi2d(i))
    ct5 = psi2(i)*xid(i) - mr*xi(i)*psi2d(i)
    an(i) = (mr*ct2 - ct) / ct4
    bn(i) = (ct2 - mr*ct)/ct5
    cn(i) = ct3/ct5
    dn(i) = ct3/ct4
  END DO

 
END subroutine absMie


!******* Coated BHMIE ****************

SUBROUTINE BHCOAT(NSTOP, XX1 ,XX2, RRFRL1, RRFRL2 ,a_n,b_n)
!      subroutine mieSolution2(xx1,xx2,RRFRL1,RRFRL2,nang,s1,s2,QQEXT,QQSCA,QBACK)
! Arguments:

    real(kind=rk) :: XX1, XX2
      REAL(kind=rk) :: GSCA,QBACK,QQEXT,QQSCA,XX,YY
      COMPLEX(kind=rk) :: RRFRL1,RRFRL2
      integer :: nang
 !      complex(kind=rk) :: s1(:),s2(:) 
! Local variables:

      complex(kind=rk), PARAMETER :: II=dcmplx(0.0,1.D0)
      real(kind=rk), PARAMETER :: DEL=1.D-8

      INTEGER :: IFLAG,N,NSTOP, j1, jj
      real(kind=rk) :: CHI0Y,CHI1Y,CHIY,EN,PSI0Y,PSI1Y,PSIY,QEXT,QSCA,RN,X,Y,YSTOP, fn, dang, p, t
     COMPLEX(kind=rk) :: AMESS1,AMESS2,AMESS3,AMESS4,AN,AN1,ANCAP, &
        BN,BN1,BNCAP,BRACK, &
        CHI0X2,CHI0Y2,CHI1X2,CHI1Y2,CHIX2,CHIPX2,CHIPY2,CHIY2,CRACK,&
        D0X1,D0X2,D0Y2,D1X1,D1X2,D1Y2,DNBAR,GNBAR, &
        REFREL,RFREL1,RFREL2, &
        XBACK,XI0Y,XI1Y,XIY, & 
        X1,X2,Y2

     complex(kind=rk) :: a_n(NSTOP), b_n(NSTOP)
     real(kind=rk),allocatable :: amu(:),theta(:),pii(:),tau(:),pi0(:),pi1(:)

     !xx1 = xx2/2.0_rk
     !RRFRL1 = dcmplx(1.7125,0.0038)  !blue
     !RRFRL1 = dcmplx(1.7004,0.0008) ! red
     !RRFRL1 = dcmplx(1.313,1.0e-7)
!***********************************************************************
!
! Subroutine BHCOAT calculates Q_ext, Q_sca, Q_back, g=<cos> 
! for coated sphere.
! All bessel functions computed by upward recurrence.
! Input:
!        X = 2*PI*RCORE*REFMED/WAVEL
!        Y = 2*PI*RMANT*REFMED/WAVEL
!        RFREL1 = REFCOR/REFMED
!        RFREL2 = REFMAN/REFMED 
! where  REFCOR = complex refr.index of core)
!        REFMAN = complex refr.index of mantle)
!        REFMED = real refr.index of medium)
!        RCORE = radius of core
!        RMANT = radius of mantle
!        WAVEL = wavelength of light in ambient medium

! returns:
!        QQEXT = C_ext/pi*rmant^2
!        QQSCA = C_sca/pi*rmant^2
!        QBACK = 4*pi*(dQ_sca/dOmega)
!              = "radar backscattering efficiency factor"
!        GSCA  = <cos(theta)> for scattered power
!
! Routine BHCOAT is taken from Bohren & Huffman (1983)
! extended by Prof. Francis S. Binkowski of The University of North
! Carolina at Chapel Hill to evaluate GSCA=<cos(theta)>
! History:
! 92.11.24 (BTD) Explicit declaration of all variables
! 00.05.16 (BTD) Added IMPLICIT NONE
! 12.04.10 (FSB) Modified by Prof. Francis S. Binkowski of
!                The University of North Carolina at Chapel Hill
!                to evaluate GSCA=<cos(theta)>
! 12.06.15 (BTD) Cosmetic changes
!***********************************************************************

      allocate(theta(nang),amu(nang))
      allocate(pi0(nang),pi1(nang),pii(nang))
      allocate(tau(nang))
      dang=pi/(2.0_rk*(nang-1.0_rk))
       do j1=1,nang
            theta(j1)=(j1-1)*dang
            amu(j1)=cos(theta(j1))
        enddo
      
      X=XX1
      Y=XX2
      RFREL1=RRFRL1
      RFREL2=RRFRL2
!         -----------------------------------------------------------
!              del is the inner sphere convergence criterion
!         -----------------------------------------------------------
      X1=RFREL1*X
      X2=RFREL2*X
      Y2=RFREL2*Y
      YSTOP=Y+4.*Y**0.3333+2.0
      REFREL=RFREL2/RFREL1
      !NSTOP=YSTOP

    !  allocate(a_n(NSTOP), b_n(NSTOP))
!         -----------------------------------------------------------
!              series terminated after nstop terms
      !         -----------------------------------------------------------

      pi0(:)=0.0_rk
      pi1(:)=1.0_rk
    !  s1(:)=0.0_rk
    !  s2(:)=0.0_rk

      
      D0X1=COS(X1)/SIN(X1)
      D0X2=COS(X2)/SIN(X2)
      D0Y2=COS(Y2)/SIN(Y2)
      PSI0Y=COS(Y)
      PSI1Y=SIN(Y)
      CHI0Y=-SIN(Y)
      CHI1Y=COS(Y)
      XI0Y=PSI0Y-II*CHI0Y
      XI1Y=PSI1Y-II*CHI1Y
      CHI0Y2=-SIN(Y2)
      CHI1Y2=COS(Y2)
      CHI0X2=-SIN(X2)
      CHI1X2=COS(X2)
      QSCA=0.0_rk
      QEXT=0.0_rk
      XBACK=dcmplx(0.0,0.0)
      IFLAG=0
      DO N=1,NSTOP
         RN=N
         fn=(2.0_rk*RN+1.0_rk)/(RN*(RN+1))
         EN=RN
         PSIY=(2.0_rk*RN-1.0_rk)*PSI1Y/Y-PSI0Y
         CHIY=(2.0_rk*RN-1.0_rk)*CHI1Y/Y-CHI0Y
         XIY=PSIY-II*CHIY
         D1Y2=1.0_rk/(RN/Y2-D0Y2)-RN/Y2
         IF(IFLAG.EQ.0)THEN

            D1X1=1.0_rk/(RN/X1-D0X1)-RN/X1
            D1X2=1.0_rk/(RN/X2-D0X2)-RN/X2
            CHIX2=(2.0_rk*RN-1.0_rk)*CHI1X2/X2-CHI0X2
            CHIY2=(2.0_rk*RN-1.0_rk)*CHI1Y2/Y2-CHI0Y2
            CHIPX2=CHI1X2-RN*CHIX2/X2
            CHIPY2=CHI1Y2-RN*CHIY2/Y2
            ANCAP=REFREL*D1X1-D1X2
            ANCAP=ANCAP/(REFREL*D1X1*CHIX2-CHIPX2)
            ANCAP=ANCAP/(CHIX2*D1X2-CHIPX2)
            BRACK=ANCAP*(CHIY2*D1Y2-CHIPY2)
            BNCAP=REFREL*D1X2-D1X1
            BNCAP=BNCAP/(REFREL*CHIPX2-D1X1*CHIX2)
            BNCAP=BNCAP/(CHIX2*D1X2-CHIPX2)
            CRACK=BNCAP*(CHIY2*D1Y2-CHIPY2)
            AMESS1=BRACK*CHIPY2
            AMESS2=BRACK*CHIY2
            AMESS3=CRACK*CHIPY2
            AMESS4=CRACK*CHIY2

         ENDIF ! test on iflag.eq.0

         IF(ABS(AMESS1).LT.DEL*ABS(D1Y2).AND. &
           ABS(AMESS2).LT.DEL.AND. &
           ABS(AMESS3).LT.DEL*ABS(D1Y2).AND. &
           ABS(AMESS4).LT.DEL)THEN

! convergence for inner sphere

            BRACK=dcmplx(0.0,0.0)
            CRACK=dcmplx(0.0,0.0)
            IFLAG=1
         ELSE

! no convergence yet

            IFLAG=0

         ENDIF
         DNBAR=D1Y2-BRACK*CHIPY2
         DNBAR=DNBAR/(1.0_rk-BRACK*CHIY2)
         GNBAR=D1Y2-CRACK*CHIPY2
         GNBAR=GNBAR/(1.0_rk-CRACK*CHIY2)

! store previous values of an and bn for use in computation of 
! g=<cos(theta)>

         IF(N.GT.1)THEN
            AN1=AN
            BN1=BN

            a_n(N-1) = -AN
            b_n(N-1) = -BN
         ENDIF

! update an and bn

         AN=(DNBAR/RFREL2+RN/Y)*PSIY-PSI1Y
         AN=AN/((DNBAR/RFREL2+RN/Y)*XIY-XI1Y)
         BN=(RFREL2*GNBAR+RN/Y)*PSIY-PSI1Y
         BN=BN/((RFREL2*GNBAR+RN/Y)*XIY-XI1Y)

! calculate sums for qsca,qext,xback

         QSCA=QSCA+(2.0*RN+1.0)*(ABS(AN)*ABS(AN)+ABS(BN)*ABS(BN))
         XBACK=XBACK+(2.0*RN+1.0)*(-1.)**N*(AN-BN)
         QEXT=QEXT+(2.0*RN+1.0)*(DBLE(AN)+DBLE(BN))

! (FSB) calculate the sum for the asymmetry factor

         GSCA=GSCA+((2.0*EN+1.)/(EN*(EN+1.0)))* &
              (REAL(AN)*REAL(BN)+IMAG(AN)*IMAG(BN))
         
         IF(N.GT.1)THEN
            GSCA=GSCA+((EN-1.)*(EN+1.)/EN)* &
                (REAL(AN1)*REAL(AN)+IMAG(AN1)*IMAG(AN)+ &
                 REAL(BN1)*REAL(BN)+IMAG(BN1)*IMAG(BN))
         ENDIF

     !     do j1=1,nang
     !        jj=2*nang-j1
     !        pii(j1)=pi1(j1)
     !        tau(j1)=rn*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)
     !        p=(-1.0_rk)**(n-1)
     !        s1(j1)=s1(j1)+fn*(an*pii(j1)+bn*tau(j1))
     !        t=(-1.0_rk)**n
     !        s2(j1)=s2(j1)+fn*(an*tau(j1)+bn*pii(j1))
     !        if (j1/=jj) then
     !           s1(jj)=s1(jj)+fn*(an*pii(j1)*p+bn*tau(j1)*t)
     !           s2(jj)=s2(jj)+fn*(an*tau(j1)*t+bn*pii(j1)*p)
     !        endif             
     !     end do
         
! continue update for next iteration

         PSI0Y=PSI1Y
         PSI1Y=PSIY
         CHI0Y=CHI1Y
         CHI1Y=CHIY
         XI1Y=PSI1Y-II*CHI1Y
         CHI0X2=CHI1X2
         CHI1X2=CHIX2
         CHI0Y2=CHI1Y2
         CHI1Y2=CHIY2
         D0X1=D1X1
         D0X2=D1X2
         D0Y2=D1Y2

          do j1=1,nang
             pi1(j1)=((2.0_rk*rn+1.0_rk)/(rn))*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)/rn
             pi0(j1)=pii(j1)
          enddo
      ENDDO

! have summed sufficient terms
! now compute QQSCA,QQEXT,QBACK, and GSCA
      
      QQSCA=(2.0/(Y*Y))*QSCA
      QQEXT=(2.0/(Y*Y))*QEXT
      QBACK=(ABS(XBACK))**2
      QBACK=(1.0/(Y*Y))*QBACK
      GSCA=2.0*GSCA/QSCA

      deallocate(theta,amu)
      deallocate(pi0,pi1,pii)
      deallocate(tau)
      
    END SUBROUTINE bhcoat


subroutine mie_coeff_nm(N, x, mr, a_nm, b_nm, c_nm, d_nm)
integer :: N 
real(dp) :: x, x_c
complex(dp) :: mr, mr_c 
complex(dp), dimension((N+1)**2-1) :: a_nm, b_nm, c_nm, d_nm

complex(dp), dimension(N) :: a_n, b_n, c_n, d_n
integer :: las, i1, m
!complex(dp) :: mmed

!mmed = dcmplx(1.0,0.0)
!call absMie(x, mr, mmed, N+1, a_n, b_n, c_n, d_n)

call BHMIE(N+1, x, mr, a_n, b_n)

!mr_c = dcmplx(1.6,0.05)
!x_c = x/2.0_dp
!call BHCOAT(N+1, x_c ,x, mr_c, mr ,a_n,b_n)

las = 0;
do i1 = 1,N
   do  m = -i1,i1
      las = las + 1;
      a_nm(las) = a_n(i1);
      b_nm(las) = b_n(i1);
      c_nm(las) = 0.0!c_n(i1);
      d_nm(las) = 0.0!d_n(i1);
   end do
end do


end subroutine mie_coeff_nm

!______________________________________________________!!
!
! The routine computes the SVWF expansion coefficients 
! for a time harmonic x-polarized planewave
! propagating +z-direction  with the wave number k
! 
!_____________________________________________________!!
subroutine planewave(Nmax, k, a_nm, b_nm)
integer :: Nmax
complex(dp), dimension((Nmax+1)**2-1) :: a_nm, b_nm
complex(dp) :: k

integer :: ind, n, m, mm
real(dp) :: scale, C, E0, omega
complex(dp) :: q

E0 = 1
omega = k*299792458.0
ind = 0

do n = 1,Nmax

   scale = sqrt(dble(n*(n+1)))

   do m = -n, n 
      ind = ind + 1
      mm = abs(m)
      
      C = scale*E0*sqrt(pi*(2*n+1d0)) / (n*(n+1d0)) * sqrt(factorial(n+1)/factorial(n-1))

      q = -(dcmplx(0.0,1.0)**n*k)/(omega*mu)

      if(mm == 1) then 
         a_nm(ind) = dcmplx(0.0,1.0)**(n-1.0) * C
         b_nm(ind) = -dcmplx(0.0,1.0)**(n+1.0) * C

         if(m == -1) then 
            a_nm(ind) = -a_nm(ind)
         
         end if

      else 
         a_nm(ind) = dcmplx(0.0,0.0)
         b_nm(ind) = dcmplx(0.0,0.0)
      end if
        a_nm(ind) = -a_nm(ind) 
         b_nm(ind) = -b_nm(ind) 
   end do

end do


end subroutine planewave

subroutine planewave2(Nmax, k, a_nm, b_nm, a_nm2, b_nm2)
integer :: Nmax
complex(dp), dimension((Nmax+1)**2-1) :: a_nm, b_nm, a_nm2, b_nm2
complex(dp) :: k

integer :: las, n
complex(dp), dimension(:), allocatable :: rotD
integer, dimension(:,:), allocatable :: indD
call planewave(Nmax, k, a_nm, b_nm)

las = 0
do n = 1,Nmax
   las = las + (2*n+1)**2
end do

allocate(rotD(las))
allocate(indD(las,2))

call sph_rotation_sparse(0.0d0, -pi/2.0d0, Nmax, rotD, indD)
a_nm2 = sparse_matmul(rotD,indD,a_nm,(Nmax+1)**2-1)
b_nm2 = sparse_matmul(rotD,indD,b_nm,(Nmax+1)**2-1)


end subroutine planewave2
!*****************************************************************
!
! Calculates fields from coefficients 
!
! F = electric field
! G = Magnetic field
!
! Note: This cannot evaluate fields at the z-axis 
!
!*******************************************************************

subroutine calc_fields(a_nm, b_nm, k, Po, F, G, inou)
complex(dp), dimension(:) :: a_nm, b_nm
real(dp) :: Po(3)
complex(dp) :: k, F(3), G(3) 
integer :: inou
integer :: Nmax, n, m, ind, mm
real(dp) :: r, theta, phi, vec(3), q
complex(dp) :: kr, alpha, beta, gamma, cc
complex(dp), dimension(:), allocatable :: sphj, sphy, sphh
complex(dp), dimension(:), allocatable :: chf1, chd1, chf2, chd2
complex(dp) :: P(3), B(3), C(3), Y, Y1, Y2, M_nm(3), N_nm(3)
real(dp), dimension(:), allocatable :: L, L1, L2


Nmax = int(sqrt(dble(1+size(a_nm))))-1

vec = cart2sph(Po)
!print*, vec

r = vec(1)
theta = vec(2)
phi = vec(3)

if(abs(theta)<1.0d-6) theta = 1e-6

kr = k*r

!omega = real(k)*299792458.0

allocate(sphj(Nmax+2), sphy(Nmax+2), sphh(Nmax+2))
allocate(chf1(Nmax+2), chd1(Nmax+2), chf2(Nmax+2),  chd2(Nmax+2))

! spherical bessel functions at kr
if(inou == 0) then
   call cspherebessel2(Nmax+1,kr, sphj, sphy)
   sphh = sphj
else 
  
   sphh = sphankel(Nmax+1, kr)  
   !sphh = sphankel_a(Nmax+1, kr) / exp(dcmplx(0.0,1.0)*k*r) * (k*r)

   !call cspherebessel2(Nmax+1,kr, sphj, sphy)
   !sphh = sphj + dcmplx(0.0,1.0)*sphy 
   !print*, kr
   !call ch12n (Nmax+1, kr, Nmax+1, chf1, chd1, chf2, chd2 )
   !sphh = chf1
 
end if

ind = 0
F(:) = dcmplx(0.0,0.0)
G(:) = dcmplx(0.0,0.0)

do n = 1, Nmax
   alpha = sphh(n+1)
   beta = sqrt(dble(n*(n+1)))/kr * sphh(n+1)
   gamma = (n+1.0d0)/kr * sphh(n+1) - sphh(n+2)
   !gamma = - sphh(n+2)
   allocate(L(n+1),L1(n+2),L2(n))

   call legendre2(n,cos(theta),L)  
   call legendre2(n+1,cos(theta),L1)
   call legendre2(n-1,cos(theta),L2)

   q=(sqrt(n*(n+1.0d0)))/((n*2d0+1.0d0)*sin(theta));

   do m = -n, n
      ind = ind+1
      mm = abs(m)
      
      cc = sqrt((2d0*n+1.0d0)*factorial(n-mm)/factorial(n+mm)/(4d0*pi));
     
      ! Unnormalized complex scalar spherical harmonics
      Y=L(mm+1)*exp(dcmplx(0.0, m*phi));
      Y1=L1(mm+1)*exp(dcmplx(0.0, m*phi));
     
 
      if(mm == n) then
         Y2 = dcmplx(0.0,0.0)
      else 
         Y2 = L2(mm+1)*exp(dcmplx(0.0, m*phi)) 
      end if

      ! vector spherical harmonics
      P(:) = dcmplx(0.0,0.0)
      P(1) = Y

      Y1=Y1*((n-mm+1.0d0)/(n+1.0d0))

     
      Y2=Y2*(dble(n+mm)/dble(n))

      B(:) = dcmplx(0.0,0.0)
      B(2) = Y1-Y2
      B(3)=((dcmplx(0.0, m*(2*n+1.0)))/(n*(n+1.0)))*Y

      B = B*q

      C(:) = dcmplx(0.0,0.0)
      C(2) = B(3) 
      C(3) = -B(2)

      ! Spherical vector wave functions
      M_nm = cc * alpha * C
      N_nm = cc * (beta*P + gamma * B)
      
      F = F + a_nm(ind) * M_nm + b_nm(ind) * N_nm 
      G = G + k * (a_nm(ind) * N_nm + b_nm(ind) * M_nm)
     
   end do
   
   deallocate(L,L1,L2)
  
end do

F = sph2cart_vec(theta, phi, F)

G = sph2cart_vec(theta, phi, G)
!G = G/(dcmplx(0.0,1.0) * omega * mu)

end subroutine calc_fields


!*****************************************************************
!
! Calculates fields from coefficients 
!
! F = electric field
! G = Magnetic field
!
! Note: This cannot evaluate fields at the z-axis 
!
!*******************************************************************

subroutine calc_fields2(a_nm, b_nm, a_nm2, b_nm2, k, Po, F, G, F2, G2, inou, Lmat)
complex(dp), dimension(:) :: a_nm, b_nm, a_nm2, b_nm2
real(dp) :: Po(3)
complex(dp) :: k, F(3), G(3), F2(3), G2(3) 
integer :: inou
integer :: Nmax, n, m, ind, mm
real(dp) :: r, theta, phi, vec(3), q
complex(dp) :: kr, alpha, beta, gamma, cc
complex(dp), dimension(:), allocatable :: sphj, sphy, sphh
complex(dp), dimension(:), allocatable :: chf1, chd1, chf2, chd2
complex(dp) :: P(3), B(3), C(3), Y, Y1, Y2, M_nm(3), N_nm(3)
real(dp), dimension(:), allocatable :: L, L1, L2
real(dp), dimension(:,:) :: Lmat
!real(dp), dimension(:,:), allocatable :: Mmat
Nmax = int(sqrt(dble(1+size(a_nm))))-1

vec = cart2sph(Po)
!print*, vec

r = vec(1)
theta = vec(2)
phi = vec(3)

kr = k*r

!omega = real(k)*299792458.0

allocate(sphj(Nmax+2), sphy(Nmax+2), sphh(Nmax+2))
allocate(chf1(Nmax+2), chd1(Nmax+2), chf2(Nmax+2),  chd2(Nmax+2))

! spherical bessel functions at kr
if(inou == 0) then
   call cspherebessel2(Nmax+1,kr, sphj, sphy)
   sphh = sphj
else 
  
   sphh = sphankel(Nmax+1, kr)  
   !sphh = sphankel_a(Nmax+1, kr) / exp(dcmplx(0.0,1.0)*k*r) * (k*r)

   !call cspherebessel2(Nmax+1,kr, sphj, sphy)
   !sphh = sphj + dcmplx(0.0,1.0)*sphy 
   !print*, kr
   !call ch12n (Nmax+1, kr, Nmax+1, chf1, chd1, chf2, chd2 )
   !sphh = chf1
 
end if

ind = 0
F(:) = dcmplx(0.0,0.0)
G(:) = dcmplx(0.0,0.0)

F2(:) = dcmplx(0.0,0.0)
G2(:) = dcmplx(0.0,0.0)

do n = 1, Nmax
   alpha = sphh(n+1)
   beta = sqrt(dble(n*(n+1)))/kr * sphh(n+1)
   gamma = (n+1.0d0)/kr * sphh(n+1) - sphh(n+2)
   !gamma = - sphh(n+2)
   allocate(L(n+1),L1(n+2),L2(n))

   !call legendre2(n,cos(theta),L)  
   !call legendre2(n+1,cos(theta),L1)
   !call legendre2(n-1,cos(theta),L2)

   L = Lmat(1:n+1,n+1)
   L1 = Lmat(1:n+2,n+2)
   L2 = Lmat(1:n,n)

   q=(sqrt(n*(n+1.0d0)))/((n*2d0+1.0d0)*sin(theta));

   do m = -n, n
      ind = ind+1
      mm = abs(m)
      
      cc = sqrt((2d0*n+1.0d0)*factorial(n-mm)/factorial(n+mm)/(4d0*pi));
     
      ! Unnormalized complex scalar spherical harmonics
      Y=L(mm+1)*exp(dcmplx(0.0, m*phi));
      Y1=L1(mm+1)*exp(dcmplx(0.0, m*phi));
     
 
      if(mm == n) then
         Y2 = dcmplx(0.0,0.0)
      else 
         Y2 = L2(mm+1)*exp(dcmplx(0.0, m*phi)) 
      end if

      ! vector spherical harmonics
      P(:) = dcmplx(0.0,0.0)
      P(1) = Y

      Y1=Y1*((n-mm+1.0d0)/(n+1.0d0))

     
      Y2=Y2*(dble(n+mm)/dble(n))

      B(:) = dcmplx(0.0,0.0)
      B(2) = Y1-Y2
      B(3)=((dcmplx(0.0, m*(2*n+1.0)))/(n*(n+1.0)))*Y

      B = B*q

      C(:) = dcmplx(0.0,0.0)
      C(2) = B(3) 
      C(3) = -B(2)

      ! Spherical vector wave functions
      M_nm = cc * alpha * C
      N_nm = cc * (beta*P + gamma * B)
      
      F = F + a_nm(ind) * M_nm + b_nm(ind) * N_nm 
      G = G + k * (a_nm(ind) * N_nm + b_nm(ind) * M_nm)
     
      F2 = F2 + a_nm2(ind) * M_nm + b_nm2(ind) * N_nm 
      G2 = G2 + k * (a_nm2(ind) * N_nm + b_nm2(ind) * M_nm)


   end do
   
   deallocate(L,L1,L2)
  
end do

F = sph2cart_vec(theta, phi, F)
G = sph2cart_vec(theta, phi, G)

F2 = sph2cart_vec(theta, phi, F2)
G2 = sph2cart_vec(theta, phi, G2)
!G = G/(dcmplx(0.0,1.0) * omega * mu)

end subroutine calc_fields2




!*****************************************************************
!
! Calculates far fields from coefficients a_nm, b_nm
!
! Eth = theta component of the electric field * R / exp(ikR)  
! Ephi = phi component of the electric field * R / exp(ikR) 
!
! Note: This cannot evaluate fields at the z-axis 
!
!*******************************************************************

subroutine calc_farfields(a_nm, b_nm, k,r, theta, phi,F)
complex(dp), dimension(:) :: a_nm, b_nm
real(dp) :: Po(3)
complex(dp) :: k, F(3), G(3), Ephi, Eth, i
integer :: Nmax, n, m, ind, mm
real(dp) :: r, theta, phi, vec(3), q
complex(dp) :: kr, alpha, beta, gamma, cc
complex(dp), dimension(:), allocatable :: sphj, sphy, sphh
complex(dp), dimension(:), allocatable :: chf1, chd1, chf2, chd2
complex(dp) :: P(3), B(3), C(3), Y, Y1, Y2, M_nm(3), N_nm(3)
real(dp), dimension(:), allocatable :: L, L1, L2

i = dcmplx(0.0,1.0)
Nmax = int(sqrt(dble(1+size(a_nm))))-1


ind = 0
F(:) = dcmplx(0.0,0.0)
G(:) = dcmplx(0.0,0.0)

sphh = sphankel_a(Nmax+1, k*r) 

do n = 1, Nmax

   !alpha = (-i)**(n+1) * exp(i*k*r)/(k*r)!sphh(n+1)
   !gamma = -(-i)**(n+2) * exp(i*k*r)/(k*r) !(n+1.0d0)/kr * sphh(n+1) - sphh(n+2)
 
   alpha = sphh(n+1)
   gamma = -sphh(n+2)

   allocate(L(n+1),L1(n+2),L2(n))

   call legendre2(n,cos(theta),L)  
   call legendre2(n+1,cos(theta),L1)
   call legendre2(n-1,cos(theta),L2)

   q=(sqrt(n*(n+1.0d0)))/((n*2d0+1.0d0)*sin(theta));

   do m = -n, n
      ind = ind+1
      mm = abs(m)
      
      cc = sqrt((2d0*n+1.0d0)*factorial(n-mm)/factorial(n+mm)/(4d0*pi));
     
      ! Unnormalized complex scalar spherical harmonics
      Y=L(mm+1)*exp(dcmplx(0.0, m*phi));
      Y1=L1(mm+1)*exp(dcmplx(0.0, m*phi));
     
 
      if(mm == n) then
         Y2 = dcmplx(0.0,0.0)
      else 
         Y2 = L2(mm+1)*exp(dcmplx(0.0, m*phi)) 
      end if

      ! transversal vector spherical harmonics
    
      Y1=Y1*((n-mm+1.0d0)/(n+1.0d0))   
      Y2=Y2*(dble(n+mm)/dble(n))

      B(:) = dcmplx(0.0,0.0)
      B(2) = Y1-Y2
      B(3)=((dcmplx(0.0, m*(2*n+1.0)))/(n*(n+1.0)))*Y

      B = B*q

      C(:) = dcmplx(0.0,0.0)
      C(2) = B(3) 
      C(3) = -B(2)

      ! Spherical vector wave functions
      M_nm = cc * alpha * C
      N_nm = cc * (gamma * B)
      
      F = F + a_nm(ind) * M_nm + b_nm(ind) * N_nm 
      
   end do
   
   deallocate(L,L1,L2)
  
end do

Eth = F(2)
Ephi = F(3)

F = sph2cart_vec(theta, phi, F)

!G = G/(dcmplx(0.0,1.0) * omega * mu)

end subroutine calc_farfields









!**********************************************************************
!
! Computes farfields and cross sections for a cluster  
!
!**********************************************************************
subroutine compute_cluster_fields(sphere, x, rhs, k, N_theta, N_phi, E_out,crs)
type (data_struct), dimension(:) :: sphere
complex(dp), dimension(:) :: x, rhs
complex(dp) :: k
complex(dp), dimension(:,:), allocatable :: E_out

integer :: sph, a1, a2, b1, b2, Nmax, N_phi, N_theta, i1, i2, las
real(dp) :: phi, theta, r(3), RR, cp(3), Cext, Csca, Cabs, q
real(dp) :: Cext2, Csca2, Cabs2, rad, absb, rhat(3), Csca_int,crs(4)
complex(dp) :: F(3), G(3), epsr
complex(dp), dimension(:), allocatable :: c_in, d_in

allocate(E_out(3,N_theta*N_phi))
E_out(:,:) = dcmplx(0.0,0.0)

Cext = 0.0
Csca = 0.0
Cabs = 0.0
Csca_int = 0.0


!$omp parallel default(private) &
!$omp firstprivate(N_phi, N_theta, k) &
!$omp shared(Cabs, Cext, sphere,x, E_out,rhs) 
!$omp do reduction(+:E_out,Cabs,Cext) 

do sph = 1, size(sphere)
    
   a1 = sphere(sph)%ind_a1
   a2 = sphere(sph)%ind_a2
   b1 = sphere(sph)%ind_b1
   b2 = sphere(sph)%ind_b2
   cp = sphere(sph)%cp
   rad = sphere(sph)%r
   epsr = sphere(sph)%eps_r

   Nmax = sphere(sph)%Nmax
   RR = 1.0d6
   las = 1
   do i1 = 1, N_phi
      do i2 = 1, N_theta

         phi = 2*pi * (i1-1) / dble(N_phi)
         theta = pi * (i2-1) / dble(N_theta-1) 

         if(theta < 1e-6) theta = 1e-6
         if(theta > pi-1e-6) theta = pi-1e-6
         
         r(1) = RR*sin(theta)*cos(phi);
         r(2) = RR*sin(theta)*sin(phi);
         r(3) = RR*cos(theta);
              
         r = r - cp

         call calc_fields(x(a1:a2), x(b1:b2), k, r, F, G, 1)
       
         E_out(:,las) = E_out(:,las) + F

         las = las + 1
      end do
   end do

   call cross_sections(x(a1:a2), x(b1:b2), rhs(a1:a2), rhs(b1:b2), &
        k, Nmax, Cext2, Csca2, absb)

   call sphere_absorbtion2(x(a1:a2),x(b1:b2), dble(k), sphere(sph)%r, sphere(sph)%eps_r, Nmax, Cabs2)


   Cext = Cext + Cext2
   Cabs = Cabs + Cabs2

end do
!$omp end do
!$omp end parallel
Csca_int=0.0_dp
q = 0.0_dp
las=1
do i1 = 1, N_phi
   do i2 = 1, N_theta
      phi = 2*pi * (i1-1) / dble(N_phi);   
      theta = pi * (i2-1) / dble(N_theta-1);  

      if(theta < 1e-6) theta = 1e-6
      if(theta > pi-1e-6) theta = pi-1e-6
         
      
      RR = 1.0d6
      Csca_int = Csca_int + pi/2.0*4.0*pi*RR**2 * &
           dot_product(E_out(:,las),E_out(:,las)) * &
           sin(theta)/dble(N_phi)/dble(N_theta-1) 

      q = q + pi/2.0*4.0*pi*RR**2 * &
           dot_product(E_out(:,las),E_out(:,las)) * &
           sin(theta)/dble(N_phi)/dble(N_theta-1) * cos(theta)
           las = las + 1
   end do
end do

Csca = Cext - Cabs

crs=[Cext,Csca,Cabs,q/Csca_int]
print*, 'Cext', Cext
print*, 'Csca', Csca
print*, 'Cabs', Cabs
print*, 'Csca_int', Csca_int
print*, 'asym. par.', q/Csca_int




end subroutine compute_cluster_fields


!**********************************************************************
!
! Computes farfields and cross sections for a cluster  
!
!**********************************************************************
subroutine compute_cluster_farfields(sphere, x, rhs, k, N_theta, N_phi, E_out,crs)
type (data_struct), dimension(:) :: sphere
complex(dp), dimension(:) :: x, rhs
complex(dp) :: k
complex(dp), dimension(:,:), allocatable :: E_out

integer :: sph, a1, a2, b1, b2, Nmax, N_phi, N_theta, i1, i2, las
real(dp) :: phi, theta, r(3), RR, cp(3), Cext, Csca, Cabs
real(dp) :: Cext2, Csca2, Cabs2, rad, absb, rhat(3), Csca_int,crs(4)
complex(dp) :: F(3), G(3), epsr, Eth, Ephi
complex(dp), dimension(:), allocatable :: c_in, d_in

allocate(E_out(3,N_theta*N_phi))
E_out(:,:) = dcmplx(0.0,0.0)

Cext = 0.0
Csca = 0.0
Cabs = 0.0
Csca_int = 0.0


!$omp parallel default(private) &
!$omp firstprivate(N_phi, N_theta, k) &
!$omp shared(Cabs, Cext, sphere,x, E_out,rhs) 
!$omp do reduction(+:E_out,Cabs,Cext) 

do sph = 1, size(sphere)
    
   a1 = sphere(sph)%ind_a1
   a2 = sphere(sph)%ind_a2
   b1 = sphere(sph)%ind_b1
   b2 = sphere(sph)%ind_b2
   cp = sphere(sph)%cp
   rad = sphere(sph)%r
   epsr = sphere(sph)%eps_r

   Nmax = sphere(sph)%Nmax
 
   las = 1
   do i1 = 1, N_phi
      do i2 = 1, N_theta

         phi = 2*pi * (i1-1) / dble(N_phi);   
         theta = pi * (i2-1) / dble(N_theta-1);  

          if(theta < 1e-6) theta = 1e-6
          if(theta > pi-1e-6) theta = pi-1e-6
          
         RR = 1e6
         r(1) = RR*sin(theta)*cos(phi);
         r(2) = RR*sin(theta)*sin(phi);
         r(3) = RR*cos(theta);

        ! rhat = r/sqrt(dot_product(r,r))
         
         r = r - cp

         !call calc_fields(x(a1:a2), x(b1:b2), k, r, F, G, 1)
         !print*, F / exp(dcmplx(0.0,1.0)*k*RR) * (k*RR)
         call calc_farfields(x(a1:a2), x(b1:b2), k, norm(r,cp), theta, phi, F)
         !print*, F
         !stop

         E_out(:,las) = E_out(:,las) + F / exp(dcmplx(0.0,1.0)*k*RR) * (k*RR)
         !E_out(3,las) = E_out(3,las) + Ephi

         !Csca_int = Csca_int + pi/2*4*pi*RR**2*dot_product(F,F) &
         !     *sin(theta)/N_phi/N_theta
         las = las + 1
      end do
   end do

   call cross_sections(x(a1:a2), x(b1:b2), rhs(a1:a2), rhs(b1:b2), &
        k, Nmax, Cext2, Csca2, absb)

   call sphere_absorbtion2(x(a1:a2),x(b1:b2), dble(k), sphere(sph)%r, sphere(sph)%eps_r, Nmax, Cabs2)


   Cext = Cext + Cext2
   Cabs = Cabs + Cabs2

end do
!$omp end do
!$omp end parallel
Csca_int=0.0
las=1
do i1 = 1, N_phi
   do i2 = 1, N_theta
      phi = 2*pi * (i1-1) / dble(N_phi);   
      theta = pi * (i2-1) / dble(N_theta-1);  

      if(theta < 1e-6) theta = 1e-6
      if(theta > pi-1e-6) theta = pi-1e-6
      
      
      Csca_int = Csca_int + pi/2.0*4.0*pi * &
           dot_product(E_out(:,las),E_out(:,las)) * &
           sin(theta)/dble(N_phi)/dble(N_theta-1.0_dp)
         
           las = las + 1
   end do
end do

Csca = Cext - Cabs

crs=[Cext,Csca,Cabs,Csca_int]
print*, 'Cext', Cext
print*, 'Csca', Csca
print*, 'Csca_int', Csca_int
print*, 'Cabs', Cabs


end subroutine compute_cluster_farfields

subroutine mueller_matrix_far(sphere, x, x2, b_vec, b_vec2, k, N_theta, N_phi, Nmax, S_out, crs)
type (data_struct), dimension(:) :: sphere
complex(dp), dimension(:) :: x, x2
complex(dp) :: k
real(dp), dimension(:,:), allocatable :: S_out
integer :: N_phi, N_theta, Nmax
complex(dp), dimension(:) :: b_vec, b_vec2
complex(dp), dimension(:,:), allocatable :: E_out, E_out2

integer :: i1, i2, las
real(dp) :: theta, phi, abcd(2,2), r(3), RR, unit_th(3), unit_phi(3), crs(4)
real(dp) :: crs1(4),crs2(4)
complex(dp), dimension(N_phi*N_theta) :: f11,f12,f21,f22
complex(dp) ::S1, S2, S3, S4
complex(dp) :: i

i = dcmplx(0.0,1.0)

allocate(S_out(N_phi*N_theta,18))

!call inc_xy(Nmax, sphere, b_vec, b_vec2, k)
call compute_cluster_farfields(sphere, x, b_vec, k, N_theta, N_phi, E_out,crs1)
call compute_cluster_farfields(sphere, x2, b_vec2, k, N_theta, N_phi, E_out2,crs2)

crs = (crs1+crs2)/2.0_dp

las = 0
do i1 = 1, N_phi
   do i2 = 1, N_theta

      theta = pi * (i2-1) / (N_theta+1)  + pi/N_theta/2.0
      phi = 2*pi * (i1-1) / N_phi
        
      abcd(1,:) = [cos(phi), sin(phi)]
      abcd(2,:) = [sin(phi), -cos(phi)]

    
      unit_th = [cos(theta) * cos(phi), sin(phi) * cos(theta), -sin(theta)]
      unit_phi = [-sin(phi), cos(phi),0.0d0]

      !f11(las+1) = (E_out(2,las+1))
      !f21(las+1) = (E_out(3,las+1))   
      !f12(las+1) = (E_out2(2,las+1))
      !f22(las+1) = (E_out2(3,las+1))

      f11(las+1) = dot_product(E_out(:,las+1),unit_th);
      f21(las+1) = dot_product(E_out(:,las+1),unit_phi);    
      f12(las+1) = dot_product(E_out2(:,las+1),unit_th);
      f22(las+1) = dot_product(E_out2(:,las+1),unit_phi);


      S1 = -i * (f21(las+1)*abcd(1,2) + f22(las+1)*abcd(2,2))
      S2 = -i * (f11(las+1)*abcd(1,1) + f12(las+1)*abcd(2,1))
      S3 = i * (f11(las+1)*abcd(1,2) + f12(las+1)*abcd(2,2))
      S4 = i * (f21(las+1)*abcd(1,1) + f22(las+1)*abcd(2,1))

      S_out(las+1,1) = phi
      S_out(las+1,2) = theta

      ! Mueller matrix
      S_out(las+1,3)=(abs(S1)**2 + abs(S2)**2 + abs(S3)**2 + abs(S4)**2)/2.0 !S11
      S_out(las+1,4)=(abs(S2)**2 - abs(S1)**2 + abs(S4)**2 - abs(S3)**2)/2.0 !S12
      S_out(las+1,5) = -real(S2*conjg(S3) + S1*conjg(S4)) !S13
      S_out(las+1,6) = -imag(S2*conjg(S3) - S1*conjg(S4)) !S14
      S_out(las+1,7)=(abs(S2)**2 - abs(S1)**2 + abs(S3)**2 - abs(S4)**2)/2.0 !S21
      S_out(las+1,8)=(abs(S1)**2 - abs(S3)**2 - abs(S4)**2 + abs(S2)**2)/2.0 !S22
      S_out(las+1,9) = real(S2*conjg(S3) - S1*conjg(S4)) !S23
      S_out(las+1,10) = -imag(S2*conjg(S3) + S1*conjg(S4)) !S24
      S_out(las+1,11) = real(S2*conjg(S4) + S1*conjg(S3)) !S31
      S_out(las+1,12) = -real(S2*conjg(S4) - S1*conjg(S3)) !S32
      S_out(las+1,13) = -real(S1*conjg(S2) + S3*conjg(S4)) !S33
      S_out(las+1,14) = imag(S2*conjg(S1) + S4*conjg(S3)) ! S34 
      S_out(las+1,15) = imag(S4*conjg(S2) + S1*conjg(S3)) ! S41 
      S_out(las+1,16) = imag(S4*conjg(S2) - S1*conjg(S3)) ! S42 
      S_out(las+1,17) = imag(S1*conjg(S2) - S3*conjg(S4)) ! S43 
      S_out(las+1,18) = -real(S1*conjg(S2) - S3*conjg(S4)) ! S44

      las = las + 1 
   end do
end do





end subroutine mueller_matrix_far


subroutine mueller_matrix(sphere, x, x2, b_vec, b_vec2, k, N_theta, N_phi, Nmax, S_out, J_out, crs)
type (data_struct), dimension(:) :: sphere
complex(dp), dimension(:) :: x, x2
complex(dp) :: k
real(dp), dimension(:,:), allocatable :: S_out
integer :: N_phi, N_theta, Nmax
complex(dp), dimension(:) :: b_vec, b_vec2
complex(dp), dimension(:,:), allocatable :: E_out, E_out2, J_out

integer :: i1, i2, las
real(dp) :: theta, phi, abcd(2,2), r(3), RR, unit_th(3), unit_phi(3), crs(4)
real(dp) :: crs1(4),crs2(4)
complex(dp), dimension(N_phi*N_theta) :: f11,f12,f21,f22
complex(dp) ::S1, S2, S3, S4
complex(dp) :: i

i = dcmplx(0.0,1.0)
RR = 1.0d6
allocate(S_out(N_phi*N_theta,18))
allocate(J_out(N_phi*N_theta,6))
!call inc_xy(Nmax, sphere, b_vec, b_vec2, k)
call compute_cluster_fields(sphere, x, b_vec, k, N_theta, N_phi, E_out,crs1)
call compute_cluster_fields(sphere, x2, b_vec2, k, N_theta, N_phi, E_out2,crs2)

J_out(:,:) = dcmplx(0.0,0.0)

crs = (crs1+crs2)/2

las = 0
do i1 = 1, N_phi
   do i2 = 1, N_theta

      theta = pi * (i2-1) / (N_theta-1) ! + pi/N_theta/2.0
      phi = 2*pi * (i1-1) / N_phi
        
      abcd(1,:) = [cos(phi), sin(phi)]
      abcd(2,:) = [sin(phi), -cos(phi)]

      r(1) = RR*sin(theta)*cos(phi)
      r(2) = RR*sin(theta)*sin(phi)
      r(3) = RR*cos(theta)

      unit_th = [cos(theta) * cos(phi), sin(phi) * cos(theta), -sin(theta)]
      unit_phi = [-sin(phi), cos(phi),0.0d0];

      f11(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out(:,las+1),unit_th);
      f21(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out(:,las+1),unit_phi);    
      f12(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out2(:,las+1),unit_th);
      f22(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out2(:,las+1),unit_phi);

      S1 = -i * (f21(las+1)*abcd(1,2) + f22(las+1)*abcd(2,2))
      S2 = -i * (f11(las+1)*abcd(1,1) + f12(las+1)*abcd(2,1))
      S3 = i * (f11(las+1)*abcd(1,2) + f12(las+1)*abcd(2,2))
      S4 = i * (f21(las+1)*abcd(1,1) + f22(las+1)*abcd(2,1))

      S_out(las+1,1) = phi
      S_out(las+1,2) = theta

      ! Mueller matrix
      S_out(las+1,3)=(abs(S1)**2 + abs(S2)**2 + abs(S3)**2 + abs(S4)**2)/2.0 !S11
      S_out(las+1,4)=(abs(S2)**2 - abs(S1)**2 + abs(S4)**2 - abs(S3)**2)/2.0 !S12
      S_out(las+1,5) = -real(S2*conjg(S3) + S1*conjg(S4)) !S13
      S_out(las+1,6) = -imag(S2*conjg(S3) - S1*conjg(S4)) !S14
      S_out(las+1,7)=(abs(S2)**2 - abs(S1)**2 + abs(S3)**2 - abs(S4)**2)/2.0 !S21
      S_out(las+1,8)=(abs(S1)**2 - abs(S3)**2 - abs(S4)**2 + abs(S2)**2)/2.0 !S22
      S_out(las+1,9) = real(S2*conjg(S3) - S1*conjg(S4)) !S23
      S_out(las+1,10) = -imag(S2*conjg(S3) + S1*conjg(S4)) !S24
      S_out(las+1,11) = real(S2*conjg(S4) + S1*conjg(S3)) !S31
      S_out(las+1,12) = -real(S2*conjg(S4) - S1*conjg(S3)) !S32
      S_out(las+1,13) = -real(S1*conjg(S2) + S3*conjg(S4)) !S33
      S_out(las+1,14) = imag(S2*conjg(S1) + S4*conjg(S3)) ! S34 
      S_out(las+1,15) = imag(S4*conjg(S2) + S1*conjg(S3)) ! S41 
      S_out(las+1,16) = imag(S4*conjg(S2) - S1*conjg(S3)) ! S42 
      S_out(las+1,17) = imag(S1*conjg(S2) - S3*conjg(S4)) ! S43 
      S_out(las+1,18) = -real(S1*conjg(S2) - S3*conjg(S4)) ! S44

      
      !jones matrix
      

      J_out(las+1,1) = phi
      J_out(las+1,2) = theta
   
      J_out(las+1,3) = S1
      J_out(las+1,4) = S2
      J_out(las+1,5) = S3
      J_out(las+1,6) = S4
      


      
      las = las + 1 
   end do
end do





end subroutine mueller_matrix



subroutine mueller_matrix_coeff(a_nm, b_nm, a_nm2, b_nm2, k, N_theta, N_phi, Nmax, S_out, J_out)

complex(dp), dimension(:) :: a_nm, b_nm, a_nm2, b_nm2
complex(dp) :: k
real(dp), dimension(:,:), allocatable :: S_out
complex(dp), dimension(:,:), allocatable :: J_out
integer :: N_phi, N_theta, Nmax, n
complex(dp), dimension(3) :: E_out, E_out2, H_out, H_out2

integer :: i1, i2, las
real(dp) :: theta, phi, abcd(2,2), r(3), RR, unit_th(3), unit_phi(3)
complex(dp), dimension(N_phi*N_theta) :: f11,f12,f21,f22
complex(dp) ::S1, S2, S3, S4
complex(dp) :: i
real(dp), dimension(:), allocatable :: L
real(dp), dimension(:,:), allocatable :: Lmat

i = dcmplx(0.0,1.0)
RR = 1.0d9
allocate(S_out(N_phi*N_theta,18))
allocate(J_out(N_phi*N_theta,6))

allocate(Lmat(Nmax+2,Nmax+2))
   
!!$omp parallel default(private) &
!!$omp firstprivate(N_phi, N_theta, k, Nmax, RR) &
!!$omp firstprivate(a_nm, b_nm, a_nm2, b_nm2,i) &
!!$omp shared(S_out, J_out) 
!!$omp do 
do i2 = 1, N_theta

   theta = pi * (i2-1) / (N_theta+1)  + pi/N_theta/2.0

   if(abs(theta)<1.0d-3) theta = 1e-3

  
   do n = 0, Nmax+1
      allocate(L(n+1))
      call legendre2(n,cos(theta),L)
      Lmat(1:n+1,n+1) = L
      deallocate(L)
   end do


   do i1 = 1, N_phi
 
      phi = 2*pi * (i1-1) / N_phi
        
      abcd(1,:) = [cos(phi), sin(phi)]
      abcd(2,:) = [sin(phi), -cos(phi)]

      r(1) = RR*sin(theta)*cos(phi)
      r(2) = RR*sin(theta)*sin(phi)
      r(3) = RR*cos(theta)

      call calc_fields2(a_nm, b_nm, a_nm2, b_nm2, k, r, &
           E_out, H_out, E_out2, H_out2, 1,Lmat)
      !call calc_fields2(a_nm2, b_nm2, k, r, E_out2, H_out2, 1,Lmat)

      
      unit_th = [cos(theta) * cos(phi), sin(phi) * cos(theta), -sin(theta)]
      unit_phi = [-sin(phi), cos(phi),0.0d0];

      las = i2 + (i1-1) * N_theta - 1 

      f11(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out,unit_th);
      f21(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out,unit_phi);    
      f12(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out2,unit_th);
      f22(las+1) = k*RR / exp(i*k*(RR)) * dot_product(E_out2,unit_phi);

      S1 = -i * (f21(las+1)*abcd(1,2) + f22(las+1)*abcd(2,2))
      S2 = -i * (f11(las+1)*abcd(1,1) + f12(las+1)*abcd(2,1))
      S3 = i * (f11(las+1)*abcd(1,2) + f12(las+1)*abcd(2,2))
      S4 = i * (f21(las+1)*abcd(1,1) + f22(las+1)*abcd(2,1))

      S_out(las+1,1) = phi
      S_out(las+1,2) = theta

      ! Mueller matrix
      S_out(las+1,3)=(abs(S1)**2 + abs(S2)**2 + abs(S3)**2 + abs(S4)**2)/2.0 !S11
      S_out(las+1,4)=(abs(S2)**2 - abs(S1)**2 + abs(S4)**2 - abs(S3)**2)/2.0 !S12
      S_out(las+1,5) = -real(S2*conjg(S3) + S1*conjg(S4)) !S13
      S_out(las+1,6) = -imag(S2*conjg(S3) - S1*conjg(S4)) !S14
      S_out(las+1,7)=(abs(S2)**2 - abs(S1)**2 + abs(S3)**2 - abs(S4)**2)/2.0 !S21
      S_out(las+1,8)=(abs(S1)**2 - abs(S3)**2 - abs(S4)**2 + abs(S2)**2)/2.0 !S22
      S_out(las+1,9) = real(S2*conjg(S3) - S1*conjg(S4)) !S23
      S_out(las+1,10) = -imag(S2*conjg(S3) + S1*conjg(S4)) !S24
      S_out(las+1,11) = real(S2*conjg(S4) + S1*conjg(S3)) !S31
      S_out(las+1,12) = -real(S2*conjg(S4) - S1*conjg(S3)) !S32
      S_out(las+1,13) = -real(S1*conjg(S2) + S3*conjg(S4)) !S33
      S_out(las+1,14) = imag(S2*conjg(S1) + S4*conjg(S3)) ! S34 
      S_out(las+1,15) = imag(S4*conjg(S2) + S1*conjg(S3)) ! S41 
      S_out(las+1,16) = imag(S4*conjg(S2) - S1*conjg(S3)) ! S42 
      S_out(las+1,17) = imag(S1*conjg(S2) - S3*conjg(S4)) ! S43 
      S_out(las+1,18) = -real(S1*conjg(S2) - S3*conjg(S4)) ! S44

      !jones matrix
      

      J_out(las+1,1) = phi
      J_out(las+1,2) = theta
   
      J_out(las+1,3) = S1
      J_out(las+1,4) = S2
      J_out(las+1,5) = S3
      J_out(las+1,6) = S4
      
   end do
end do
!!$omp end do
!!$omp end parallel

end subroutine mueller_matrix_coeff


subroutine tr_T(Taa, Tbb, Tab, Tba, k, crs)

complex(dp), dimension(:,:) :: Taa, Tbb, Tab, Tba
integer :: nm, mu, nu
real(dp) :: T1, T2, crs(4)
real(dp) :: k

nm = size(Taa,1)

T1 = dcmplx(0.0,0.0)
T2 = dcmplx(0.0,0.0)

do mu = 1, nm

   T1 = T1 + (real(Taa(mu, mu)) + real(Tbb(mu, mu)) + real(Tab(mu, mu)) + real(Tba(mu, mu))) 

   do nu = 1,nm

      T2 = T2 + (abs(Taa(mu, nu))**2 + abs(Tbb(mu, nu))**2 + &
           abs(Tab(mu, nu))**2 + abs(Tba(mu, nu))**2)
   
   end do
end do

crs(1) = -2*pi*T1/k**2
crs(2) = 2*pi*T2/k**2
crs(3) = (-2*pi*T1/k**2-2*pi*T2/k**2)
crs(4) = T1 + T2

print*,'Ave. Extinction cross section:', -2*pi*T1/k**2 
print*,'Ave. Scattering cross section:', 2*pi*T2/k**2
print*,'Ave. absorption cross section:', -2*pi*T1/k**2 -2*pi*T2/k**2
print*,'Tr_(real(T) + T adj(T)):', T1 + T2


end subroutine tr_T

end module mie

